package viked.geneticalgorithm.view.fragments.preference

import android.os.Bundle
import android.support.v7.preference.PreferenceDialogFragmentCompat
import android.view.View
import android.widget.NumberPicker
import android.widget.TextView
import viked.geneticalgorithm.R
import viked.geneticalgorithm.model.preferences.custom.NumericalPreference

/**
 * Created by 1 on 24.03.2016.
 */
class NumericalDialogFragmentCompat : PreferenceDialogFragmentCompat() {

    companion object {

        private val VALUE_KEY = "text"

        fun newInstance(key: String): NumericalDialogFragmentCompat {
            val fragment = NumericalDialogFragmentCompat()
            val b = Bundle(1)
            b.putString("key", key)
            fragment.arguments = b
            return fragment
        }
    }

    var value = 0

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            value = this.getNumericalPreference().value
        } else {
            value = (savedInstanceState.getInt(VALUE_KEY))
        }
    }

    private fun getNumericalPreference(): NumericalPreference {
        return this.preference as NumericalPreference
    }

    override fun onBindDialogView(view: View?) {
        super.onBindDialogView(view)
        (view?.findViewById(R.id.dialog_title) as TextView).text = getNumericalPreference().title
        val numberPicker = view?.findViewById(R.id.numberPicker) as NumberPicker
        numberPicker.minValue = getNumericalPreference().min
        numberPicker.maxValue = getNumericalPreference().max
        numberPicker.value = getNumericalPreference().value
        numberPicker.setOnValueChangedListener { picker, i, j -> if(i!= j){
            value = j} }
    }



    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(VALUE_KEY, value)
    }


    override fun onDialogClosed(p0: Boolean) {
        if (p0) {
            val preference = getNumericalPreference()
            if (preference.callChangeListener(value)) {
                preference.value = value
            }
        }
    }

}