package viked.geneticalgorithm.view.fragments.result.ration

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_result.*
import viked.geneticalgorithm.R
import viked.geneticalgorithm.di.view.fragment.tabs.result.ResultFragmentComponent
import viked.geneticalgorithm.di.view.fragment.tabs.result.ResultFragmentModule
import viked.geneticalgorithm.model.genetic.algorithm.objects.Ration
import viked.geneticalgorithm.presenter.fragments.result.IResultPresenter
import viked.geneticalgorithm.presenter.fragments.result.ResultPresenterImpl
import viked.geneticalgorithm.view.adapters.RationListAdapter
import viked.geneticalgorithm.view.fragments.tabs.TabsFragment
import javax.inject.Inject
import kotlin.properties.Delegates

/**
 * Created by 1 on 29.03.2016.
 */
class ResultRationFragment : Fragment(), IResultRationView {

    val SELECTION_KEY = "selection"

    lateinit var adapterRation: RationListAdapter

    var component: ResultFragmentComponent by Delegates.notNull<ResultFragmentComponent>()

    @Inject
    lateinit var presenter: IResultPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }


    override fun updateList(list: List<Ration>) {
        adapterRation.updateList(list, null)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        component = TabsFragment.getInstance(fragmentManager!!).component.plusResultFragmentComponent(ResultFragmentModule(this))
        component.inject(this)

        setupRecyclerView(savedInstanceState?.getIntArray(SELECTION_KEY))

        setProgressBarVisibility(presenter.isRunning())

        algorithmProgressBar.max = 100
    }

    private fun setupRecyclerView(intArray: IntArray?) {
        adapterRation = RationListAdapter { presenter.onShare(it) }
        adapterRation.updateList(Ration.getAll(Realm.getDefaultInstance()), intArray)
        resultRecyclerView.adapter = adapterRation
        //resultRecyclerView.addOnScrollListener(ProgressbarAnimationHelper(algorithmProgressLayout, { presenter.isRunning() }))
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putIntArray(SELECTION_KEY, adapterRation.getSelectionArray())
        super.onSaveInstanceState(outState)
    }

    override fun injectPresenter(presenter: IResultPresenter) {
        component.inject(presenter as ResultPresenterImpl)
    }

    override fun updateProgress(progress: Int, massage: String) {
        algorithmProgressBar.progress = progress
        algorithmTextView.text = massage
    }

    override fun setProgressBarVisibility(visibility: Boolean) {
        algorithmProgressLayout.visibility = if (visibility) View.VISIBLE else View.GONE
    }

    override fun context(): Context {
        return activity!!
    }
}