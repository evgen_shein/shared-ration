package viked.geneticalgorithm.view.fragments.preference

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.preference.*
import android.support.v7.preference.Preference.OnPreferenceClickListener
import viked.geneticalgorithm.R
import viked.geneticalgorithm.di.view.fragment.preferences.PreferencesFragmentComponent
import viked.geneticalgorithm.di.view.fragment.preferences.PreferencesFragmentModule
import viked.geneticalgorithm.model.preferences.*
import viked.geneticalgorithm.model.preferences.custom.BigNumberPreference
import viked.geneticalgorithm.model.preferences.custom.NumericalListPreference
import viked.geneticalgorithm.model.preferences.custom.NumericalPreference
import viked.geneticalgorithm.presenter.fragments.preferences.IPreferencePresenter
import viked.geneticalgorithm.presenter.fragments.preferences.PreferencesPresenterImpl
import viked.geneticalgorithm.view.MainActivity
import javax.inject.Inject
import kotlin.properties.Delegates

/**
 * Created by 1 on 11.04.2016.
 */
class PreferencesFragment : PreferenceFragmentCompat(), IPreferencesView, SharedPreferences.OnSharedPreferenceChangeListener {

    var component: PreferencesFragmentComponent by Delegates.notNull<PreferencesFragmentComponent>()

    @Inject
    lateinit var presenter: IPreferencePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        component = (activity as MainActivity).component.plusPreferencesFragmentComponent(PreferencesFragmentModule(this))
        component.inject(this)
        super.onCreate(savedInstanceState)
    }


    override fun onCreatePreferences(p0: Bundle?, p1: String?) {

        preferenceScreen = preferenceManager.createPreferenceScreen(context)

        val humanPreferencesGroup = PreferenceCategory(context)
        humanPreferencesGroup.title = context!!.getString(R.string.preference_group_human_title)
        preferenceScreen.addPreference(humanPreferencesGroup)

        val energyConsumptionPreference = BigNumberPreference(context!!)
        energyConsumptionPreference.title = context!!.getString(R.string.preference_energy_consumption_title)
        energyConsumptionPreference.key = ENERGY_CONSUMPTION_KEY
        humanPreferencesGroup.addPreference(energyConsumptionPreference)

        val baseSummary = context!!.getString(R.string.preference_meals_summary)
        initialGroupOfNumericalPreferences(humanPreferencesGroup,
                listOf(FIRST_MEAL_CHANCE_KEY, SECOND_MEAL_CHANCE_KEY, VEGETABLES_CHANCE_KEY, FRUIT_CHANCE_KEY, BEVARAGES_CHANCE_KEY),
                listOf(context!!.getString(R.string.preference_first_meal_title),
                        context!!.getString(R.string.preference_second_meal_title),
                        context!!.getString(R.string.preference_vegetables_title),
                        context!!.getString(R.string.preference_fruit_title),
                        context!!.getString(R.string.preference_bevarages_title)),
                listOf(baseSummary,
                        baseSummary, baseSummary, baseSummary, baseSummary))

        val weightUnitPreference = NumericalListPreference(context!!)
        weightUnitPreference.title = context!!.getString(R.string.preference_weight_unit_title)
        weightUnitPreference.intEntries = weightUnitList
        weightUnitPreference.key = WEIGHT_UNIT_KEY
        humanPreferencesGroup.addPreference(weightUnitPreference)

        val algorithmPreferencesGroup = PreferenceCategory(context)
        algorithmPreferencesGroup.title = context!!.getString(R.string.preference_group_algorithm_title)
        preferenceScreen.addPreference(algorithmPreferencesGroup)

        val populationSizePreference = NumericalListPreference(context!!)
        populationSizePreference.title = context!!.getString(R.string.preference_population_size_title)
        populationSizePreference.intEntries = populationSizeList
        populationSizePreference.key = POPULATION_SIZE_KEY
        algorithmPreferencesGroup.addPreference(populationSizePreference)

        initialGroupOfNumericalPreferences(algorithmPreferencesGroup,
                listOf(CALORIES_TOLERANCE_KEY, HARD_MUTATION_RATE_KEY, NEW_INDIVIDUALS_KEY),
                listOf(context!!.getString(R.string.preference_error_title),
                        context!!.getString(R.string.preference_mutation_rate_title),
                        context!!.getString(R.string.preference_new_individuals_title)),
                listOf("", "", ""))


        val reset = Preference(context)
        reset.title = context!!.getString(R.string.preference_reset)
        reset.onPreferenceClickListener =
                OnPreferenceClickListener {
                    presenter.restoreDefaultPreferences(preferenceManager)
                    true
                }
        preferenceScreen.addPreference(reset)
    }

    override fun onDisplayPreferenceDialog(preference: Preference?) {
        if (this.fragmentManager!!.findFragmentByTag("android.support.v7.preference.PreferenceFragment.DIALOG") == null) {
            val f: Any
            when (preference) {
                is NumericalPreference -> f = NumericalDialogFragmentCompat.newInstance(preference.getKey())
                is NumericalListPreference -> f = NumericalListDialogFragmentCompat.newInstance(preference.getKey())
                is BigNumberPreference -> f = BigNumberDialogFragmentCompat.newInstance(preference.getKey())
                is EditTextPreference -> f = EditTextPreferenceDialogFragmentCompat.newInstance(preference.getKey())
                is ListPreference -> f = ListPreferenceDialogFragmentCompat.newInstance(preference.getKey())
                else -> throw IllegalArgumentException("Tried to display dialog for unknown preference type. Did you forget to override onDisplayPreferenceDialog()?")
            }

            (f as DialogFragment).setTargetFragment(this, 0)
            f.show(this.fragmentManager, "android.support.v7.preference.PreferenceFragment.DIALOG")
        }
    }

    private fun initialGroupOfNumericalPreferences(group: PreferenceGroup,
                                                   numericalPreferencesKeysList: List<String>,
                                                   numericalPreferencesTitlesList: List<String>,
                                                   baseSummaryList: List<String>) {
        numericalPreferencesKeysList.forEachIndexed { i, s ->
            val chancePreference = NumericalPreference(context!!)
            chancePreference.key = s
            chancePreference.title = numericalPreferencesTitlesList[i]
            chancePreference.baseSummary = baseSummaryList[i]
            group.addPreference(chancePreference)

        }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        presenter.updatePreference(sharedPreferences, key)
    }

    override fun injectPresenter(presenter: IPreferencePresenter) {
        component.inject(presenter as PreferencesPresenterImpl)
    }

    override fun onPause() {
        super.onPause()
        preferenceManager.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onResume() {
        super.onResume()
        preferenceManager.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun context(): Context {
        return activity!!
    }
}