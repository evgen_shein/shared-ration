package viked.geneticalgorithm.view.fragments.result.ration

import viked.geneticalgorithm.model.genetic.algorithm.objects.Ration
import viked.geneticalgorithm.presenter.fragments.result.IResultPresenter
import viked.geneticalgorithm.view.fragments.IBaseFragmentView

/**
 * Created by 1 on 29.03.2016.
 */
interface IResultRationView : IBaseFragmentView<IResultPresenter> {
    fun updateList(list: List<Ration>)
    fun updateProgress(progress: Int, massage: String)
    fun setProgressBarVisibility(visibility: Boolean)
    fun isVisible(): Boolean
}