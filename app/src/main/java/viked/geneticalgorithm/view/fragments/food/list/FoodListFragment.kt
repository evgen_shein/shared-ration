package viked.geneticalgorithm.view.fragments.food.list

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.list_fragment.*
import viked.geneticalgorithm.R
import viked.geneticalgorithm.di.view.fragment.tabs.food.FoodFragmentComponent
import viked.geneticalgorithm.di.view.fragment.tabs.food.FoodFragmentModule
import viked.geneticalgorithm.model.data.containers.DishContainer
import viked.geneticalgorithm.presenter.fragments.food.FoodPresenterImpl
import viked.geneticalgorithm.presenter.fragments.food.IFoodPresenter
import viked.geneticalgorithm.view.adapters.DishListAdapter
import viked.geneticalgorithm.view.fragments.tabs.TabsFragment
import javax.inject.Inject
import kotlin.properties.Delegates

/**
 * Created by 1 on 29.03.2016.
 */
class FoodListFragment : Fragment(), IFoodListView {

    private val adPeriod = 0

    lateinit var adapter: DishListAdapter

    var component: FoodFragmentComponent by Delegates.notNull<FoodFragmentComponent>()

    @Inject
    lateinit var presenter: IFoodPresenter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        lv.layoutManager = layoutManager
    }

    override fun updateList(list: List<DishContainer>) {
        adapter.update(list)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        component = TabsFragment.getInstance(fragmentManager!!).component.plusFoodFragmentComponent(FoodFragmentModule(this))
        component.inject(this)

        adapter = DishListAdapter(adPeriod, presenter)
        lv.adapter = adapter
    }

    override fun onResume() {
        super.onResume()
        presenter.updateView()
    }

    override fun injectPresenter(presenter: IFoodPresenter) {
        component.inject(presenter as FoodPresenterImpl)
    }

    override fun context(): Context = context!!
}