package viked.geneticalgorithm.view.fragments.tabs

import viked.geneticalgorithm.presenter.fragments.tabs.ITabPresenter
import viked.geneticalgorithm.view.fragments.IBaseFragmentView

/**
 * Created by Viked on 7/27/2016.
 */
interface ITabView : IBaseFragmentView<ITabPresenter> {
    fun onSelectTab(position: Int)
    fun getSelectedTab(): Int
}