package viked.geneticalgorithm.view.fragments.tabs

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_tabs.*
import viked.geneticalgorithm.R
import viked.geneticalgorithm.di.view.fragment.tabs.TabFragmentComponent
import viked.geneticalgorithm.di.view.fragment.tabs.TabFragmentModule
import viked.geneticalgorithm.presenter.fragments.tabs.ITabPresenter
import viked.geneticalgorithm.presenter.fragments.tabs.TabPresenterImpl
import viked.geneticalgorithm.view.FOOD_LIST_FRAGMENT_INDEX
import viked.geneticalgorithm.view.IView
import viked.geneticalgorithm.view.MainActivity
import javax.inject.Inject
import kotlin.properties.Delegates

/**
 * Created by Viked on 7/27/2016.
 */
class TabsFragment : Fragment(), ITabView, ViewPager.OnPageChangeListener {

    companion object {
        private val SELECTED_PAGE_EXTRA = "selected_page"

        val TAG = "TabsFragment"

        fun newInstance(position: Int): TabsFragment {
            val arguments = Bundle()
            arguments.putInt(SELECTED_PAGE_EXTRA, position)
            val fragment = TabsFragment()
            fragment.arguments = arguments
            return fragment
        }

        fun getInstance(fm: FragmentManager): TabsFragment {
            return fm.findFragmentByTag(TAG) as TabsFragment
        }
    }

    var component: TabFragmentComponent by Delegates.notNull<TabFragmentComponent>()

    @Inject
    lateinit var presenter: ITabPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tabs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabLayout.setupWithViewPager(viewPager)
        viewPager.adapter = TabsPageAdapter(fragmentManager!!)
        viewPager.addOnPageChangeListener(this)
        viewPager.currentItem = arguments?.getInt(SELECTED_PAGE_EXTRA) ?: FOOD_LIST_FRAGMENT_INDEX
    }

    override fun onSelectTab(position: Int) {
        viewPager.currentItem = position
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        component = (activity as MainActivity).component.plusTabFragmentComponent(TabFragmentModule(this))
        component.inject(this)
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        (activity as IView).onPageSelected(position)
    }

    override fun injectPresenter(presenter: ITabPresenter) {
        component.inject(presenter as TabPresenterImpl)
    }

    override fun getSelectedTab(): Int = viewPager?.currentItem ?: FOOD_LIST_FRAGMENT_INDEX

    inner class TabsPageAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            return if (position == 0) {
                presenter.getFoodFragment()
            } else {
                presenter.getResultFragment()
            }
        }

        override fun getCount(): Int = 2

        override fun getPageTitle(position: Int): CharSequence {
            return if (position == 0) {
                context!!.getString(R.string.food_list)
            } else {
                context!!.getString(R.string.ration)
            }
        }
    }

    override fun context(): Context {
        return activity!!
    }
}