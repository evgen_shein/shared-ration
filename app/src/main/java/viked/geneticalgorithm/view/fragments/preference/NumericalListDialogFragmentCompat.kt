package viked.geneticalgorithm.view.fragments.preference

import android.os.Bundle
import android.support.v7.preference.PreferenceDialogFragmentCompat
import android.view.View
import android.widget.NumberPicker
import android.widget.TextView
import viked.geneticalgorithm.R
import viked.geneticalgorithm.model.preferences.custom.NumericalListPreference
import viked.geneticalgorithm.model.preferences.custom.NumericalPreference

/**
 * Created by 1 on 24.03.2016.
 */
class NumericalListDialogFragmentCompat : PreferenceDialogFragmentCompat() {

    companion object {

        private val VALUE_KEY = "text"

        fun newInstance(key: String): NumericalListDialogFragmentCompat {
            val fragment = NumericalListDialogFragmentCompat()
            val b = Bundle(1)
            b.putString("key", key)
            fragment.arguments = b
            return fragment
        }
    }

    var value = 0

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            value = this.getNumericalListPreference().value
        } else {
            value = (savedInstanceState.getInt(VALUE_KEY))
        }
    }

    private fun getNumericalListPreference(): NumericalListPreference {
        return this.preference as NumericalListPreference
    }

    override fun onBindDialogView(view: View?) {
        super.onBindDialogView(view)
        (view?.findViewById(R.id.dialog_title) as TextView).text = getNumericalListPreference().title
        val pref = getNumericalListPreference()
        val numberPicker = view?.findViewById(R.id.numberPicker) as NumberPicker
        numberPicker.minValue = 0
        numberPicker.maxValue = pref.intEntries.size-1
        numberPicker.value = pref.getIndex()
        numberPicker.displayedValues = pref.entries
        numberPicker.setOnValueChangedListener { picker, i, j -> if(i!= j){
            value = j} }
    }



    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(VALUE_KEY, value)
    }


    override fun onDialogClosed(p0: Boolean) {
        if (p0) {
            val preference = getNumericalListPreference()
            if (preference.callChangeListener(value)) {
                preference.setValueByIndex(value)
            }
        }
    }

}