package viked.geneticalgorithm.view.fragments.food.list

import viked.geneticalgorithm.model.data.containers.DishContainer
import viked.geneticalgorithm.presenter.fragments.food.IFoodPresenter
import viked.geneticalgorithm.view.fragments.IBaseFragmentView

/**
 * Created by 1 on 29.03.2016.
 */
interface IFoodListView : IBaseFragmentView<IFoodPresenter>{
    fun updateList(list:List<DishContainer>)
}