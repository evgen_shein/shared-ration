package viked.geneticalgorithm.view.fragments.preference

import android.os.Bundle
import android.support.v7.preference.PreferenceDialogFragmentCompat
import android.view.View
import android.widget.NumberPicker
import android.widget.TextView
import viked.geneticalgorithm.R
import viked.geneticalgorithm.model.preferences.custom.BigNumberPreference
import viked.geneticalgorithm.model.preferences.custom.NumericalListPreference
import viked.geneticalgorithm.model.preferences.custom.NumericalPreference

/**
 * Created by 1 on 24.03.2016.
 */
class BigNumberDialogFragmentCompat : PreferenceDialogFragmentCompat() {

    companion object {

        private val VALUE_KEY = "text"

        fun newInstance(key: String): BigNumberDialogFragmentCompat {
            val fragment = BigNumberDialogFragmentCompat()
            val b = Bundle(1)
            b.putString("key", key)
            fragment.arguments = b
            return fragment
        }
    }

    var valueList = mutableListOf(0,0,0,0)

    var numberPickerList : MutableList<NumberPicker> = mutableListOf()

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            setValueList(this.getBigNumberPreference().value)
        } else {
            setValueList(savedInstanceState.getInt(VALUE_KEY))
        }
        numberPickerList.forEachIndexed { i, numberPicker -> numberPicker.value = valueList[i] }
    }

    private fun getBigNumberPreference(): BigNumberPreference {
        return this.preference as BigNumberPreference
    }

    override fun onBindDialogView(view: View?) {
        super.onBindDialogView(view)
        (view?.findViewById(R.id.dialog_title) as TextView).text = getBigNumberPreference().title
        numberPickerList.add(view?.findViewById(R.id.numberPicker1) as NumberPicker)
        numberPickerList.add(view?.findViewById(R.id.numberPicker10) as NumberPicker)
        numberPickerList.add(view?.findViewById(R.id.numberPicker100) as NumberPicker)
        numberPickerList.add(view?.findViewById(R.id.numberPicker1k) as NumberPicker)
        numberPickerList.forEachIndexed { index, numberPicker ->
            numberPicker.minValue = 0
            numberPicker.maxValue = 9
            numberPicker.value = valueList[index]
            numberPicker.setOnValueChangedListener { picker, i, j -> if(i!= j){
                valueList[index] = j} }
        }
    }



    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(VALUE_KEY, getValue())
    }


    override fun onDialogClosed(p0: Boolean) {
        if (p0) {
            val preference = getBigNumberPreference()
            var value = getValue()
            if (preference.callChangeListener(value)) {
                preference.value = value
            }
        }
    }

    private fun getValue():Int{
        var value = 0
        for( i in valueList.size-1 downTo 0 ){
            value+= valueList[i] * Math.pow(10.0, i.toDouble()).toInt()
        }
        return value
    }

    private fun setValueList(value : Int){
        var temp = value
        for( i in valueList.size-1 downTo 0 ){
            val factor = Math.pow(10.0, i.toDouble()).toInt()
            valueList[i] = temp / factor
            temp-=valueList[i]*factor
        }
    }

}