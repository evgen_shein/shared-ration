package viked.geneticalgorithm.view.fragments.preference

import viked.geneticalgorithm.presenter.fragments.preferences.IPreferencePresenter
import viked.geneticalgorithm.view.fragments.IBaseFragmentView

/**
 * Created by 1 on 11.04.2016.
 */
interface IPreferencesView : IBaseFragmentView<IPreferencePresenter> {
}