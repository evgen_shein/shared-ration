package viked.geneticalgorithm.view.fragments.result.ration

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewTreeObserver
import android.view.animation.Animation
import android.view.animation.TranslateAnimation

/**
 * Created by Viked on 9/10/2016.
 */
class ProgressbarAnimationHelper(val progressbarLayout: View, val isVisible: () -> Boolean) : RecyclerView.OnScrollListener() {

    private val duration = 500

    private var height = 500

    init {
        progressbarLayout.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                if (progressbarLayout.visibility == View.VISIBLE) {
                    height = progressbarLayout.height
                    progressbarLayout.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }
            }
        })
    }


    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (isVisible.invoke()) {
            val visible = dy >= 0
            when {
                visible && progressbarLayout.visibility == View.GONE ->
                    progressbarLayout.startAnimation(getShowAnimation())
                visible.not() && progressbarLayout.visibility == View.VISIBLE ->
                    progressbarLayout.startAnimation(getHideAnimation())
            }
        }
    }

    private fun getShowAnimation(): Animation {
        val animation = TranslateAnimation(0f, 0f, height.toFloat(), 0f)
        animation.duration = duration.toLong()
        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(p0: Animation?) {

            }

            override fun onAnimationEnd(p0: Animation?) {

            }

            override fun onAnimationStart(p0: Animation?) {
                progressbarLayout.visibility = View.VISIBLE
            }
        })
        return animation
    }

    private fun getHideAnimation(): Animation {
        val animation = TranslateAnimation(0f, 0f, -height.toFloat(), 0f)
        animation.duration = duration.toLong()
        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(p0: Animation?) {

            }

            override fun onAnimationEnd(p0: Animation?) {
                progressbarLayout.visibility = View.GONE
            }

            override fun onAnimationStart(p0: Animation?) {

            }
        })
        return animation
    }

}