package viked.geneticalgorithm.view.fragments

import android.content.Context

/**
 * Created by Viked on 7/28/2016.
 */
interface IBaseFragmentView<in PRESENTER> {
    fun injectPresenter(presenter: PRESENTER)
    fun context(): Context
}