package viked.geneticalgorithm.view.objects

import viked.geneticalgorithm.model.genetic.algorithm.objects.Ration

/**
 * Created by Viked on 7/23/2016.
 */
class RationView(val ration: Ration) {

    constructor(ration: Ration, selected: Int) : this(ration) {
        this.selected = selected
    }

    var selected: Int = -1
}