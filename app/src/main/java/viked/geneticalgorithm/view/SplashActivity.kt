package viked.geneticalgorithm.view

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Window
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_splash.*
import viked.geneticalgorithm.MyApplication
import viked.geneticalgorithm.R
import viked.geneticalgorithm.model.VersionException
import viked.geneticalgorithm.model.data.DataBaseModel
import viked.geneticalgorithm.presenter.goToMarket
import viked.geneticalgorithm.utils.RealmUtils
import javax.inject.Inject

class SplashActivity : AppCompatActivity() {

    private val ACTION_WIFI_SETTINGS_RESULT_CODE = 0

    init {
        MyApplication.graph.inject(this)
    }

    @Inject
    lateinit var dataBaseModel: DataBaseModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_splash)
        progressbar.indeterminateDrawable.setColorFilter(
                Color.RED, android.graphics.PorterDuff.Mode.SRC_IN)
        checkData()
    }

    private fun checkData() {
        if (isConnected()) {
            dataBaseModel.load().subscribe(
                    {},
                    {
                        it.printStackTrace()
                        if (it is VersionException) {
                            showVersionErrorDialog()
                        } else {
                            Snackbar.make(progressbar, R.string.loading_error, Snackbar.LENGTH_LONG).show()
                        }
                    },
                    { startActivity() })
        } else {
            showConnectionErrorDialog()
        }
    }

    private fun startActivity() {
        val mainIntent = Intent(this, MainActivity::class.java)
        mainIntent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(mainIntent)
        finish()
    }

    private fun isConnected(): Boolean {
        val conMgr = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (conMgr != null && conMgr is ConnectivityManager) {
            val netInfo = conMgr.activeNetworkInfo
            netInfo != null && netInfo.isConnectedOrConnecting
        } else {
            false
        }
    }

    private fun showVersionErrorDialog() {
        val dialogListener: DialogInterface.OnClickListener = DialogInterface.OnClickListener { p0, p1 ->
            when (p1) {
                DialogInterface.BUTTON_POSITIVE -> goToMarket(this)
                DialogInterface.BUTTON_NEUTRAL -> startActivity()
                else -> finish()
            }
        }
        showDialog(dialogListener, R.string.dialog_version_title, R.string.dialog_version_message)
    }

    private fun showConnectionErrorDialog() {
        val dialogListener: DialogInterface.OnClickListener = DialogInterface.OnClickListener { p0, p1 ->
            when (p1) {
                DialogInterface.BUTTON_POSITIVE -> startWifiSettingsActivity()
                DialogInterface.BUTTON_NEUTRAL -> startActivity()
                else -> finish()
            }
        }
        showDialog(dialogListener, R.string.network_error_title, R.string.network_error_message)

    }

    private fun showDialog(dialogListener: DialogInterface.OnClickListener, title: Int, massage: Int) {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle(title)
        dialog.setMessage(massage)
        dialog.setPositiveButton(android.R.string.ok, dialogListener)
        dialog.setNegativeButton(android.R.string.cancel, dialogListener)
        if (RealmUtils.checkDBModel()) {
            dialog.setNeutralButton(R.string.network_error_button, dialogListener)
        }
        dialog.show()
    }

    private fun startWifiSettingsActivity() {
        startActivityForResult(Intent(Settings.ACTION_WIFI_SETTINGS), ACTION_WIFI_SETTINGS_RESULT_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ACTION_WIFI_SETTINGS_RESULT_CODE) {
            checkData()
        }
    }

}
