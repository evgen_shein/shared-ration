package viked.geneticalgorithm.view

import android.content.Context
import android.support.v4.app.FragmentManager
import viked.geneticalgorithm.presenter.IPresenter

/**
 * Created by 1 on 29.03.2016.
 */
interface IView {

    fun setFabVisibility(visibility: Boolean)

    fun setFabIcon(iconId: Int)

    fun setUserName(name: String)

    fun setUserIconId(iconId: Int)

    fun setUserIcon(imageUrl: String)

    fun setLoginButtonText(textId: Int)

    fun setSearchVisibility(visibility: Boolean)

    fun getCurrentFragment(): Int

    fun showMassage(massageErrorId: Int)

    fun getContext(): Context

    fun onPageSelected(position: Int)

    fun inject(presenter: IPresenter)

    fun getSupportFragmentManager(): FragmentManager

    fun onMassage(stringId: Int)

    fun requestAction(messageStringId: Int, actionStringId: Int, action: () -> Unit)

    fun onSelectListFragment(position: Int)

}