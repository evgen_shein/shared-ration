package viked.geneticalgorithm.view.dialogs

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import viked.geneticalgorithm.R
import viked.geneticalgorithm.view.adapters.InfoListAdapter
import kotlin.properties.Delegates

/**
 * Created by Viked on 8/24/2016.
 */
class InfoDialog : DialogFragment() {

    private val TEXT_VIEW_PADDING = 12
    private val ITEM_INDEX_KEY = "item_index_key"

    private var recyclerView: RecyclerView by Delegates.notNull<RecyclerView>()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        recyclerView = RecyclerView(context)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = InfoListAdapter(getInfoList())
        recyclerView.setHasFixedSize(true)

        val padding = getPadding()
        recyclerView.setPadding(padding, padding, padding, padding)

        recyclerView.post { recyclerView.scrollToPosition(savedInstanceState?.getInt(ITEM_INDEX_KEY) ?: 0) }

        return AlertDialog.Builder(context!!)
                .setTitle(R.string.navigation_drawer_title_info)
                .setIcon(R.drawable.information)
                .setView(recyclerView)
                .setPositiveButton(android.R.string.ok, { dialog, index -> dismiss() })
                .create()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState?.putInt(ITEM_INDEX_KEY, (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition())
    }

    private fun getInfoList(): List<InfoItem> {
        val titles = resources.getStringArray(R.array.dialog_info_titles)
        val content = resources.getStringArray(R.array.dialog_info_content)
        return titles.mapIndexed { i, s -> InfoItem(s, content[i]) }
    }

    private fun getPadding(): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, TEXT_VIEW_PADDING.toFloat(), resources.displayMetrics).toInt()
    }
}