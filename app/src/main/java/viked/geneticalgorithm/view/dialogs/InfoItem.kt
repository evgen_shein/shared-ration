package viked.geneticalgorithm.view.dialogs

/**
 * Created by Viked on 8/24/2016.
 */
class InfoItem(val title: String, val content: String)