package viked.geneticalgorithm.view

/**
 * Created by Viked on 8/29/2016.
 */
const val FOOD_LIST_FRAGMENT_INDEX = 0
const val RESULT_LIST_FRAGMENT_INDEX = 1
const val PREFERENCE_FRAGMENT_INDEX = 2