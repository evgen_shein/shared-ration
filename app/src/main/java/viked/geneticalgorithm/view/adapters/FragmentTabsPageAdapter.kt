package viked.geneticalgorithm.view.adapters

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import viked.geneticalgorithm.R
import viked.geneticalgorithm.view.fragments.food.list.FoodListFragment
import viked.geneticalgorithm.view.fragments.result.ration.ResultRationFragment

/**
 * Created by Viked on 7/27/2016.
 */
class FragmentTabsPageAdapter(val context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            FoodListFragment()
        } else {
            ResultRationFragment()
        }
    }

    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int): CharSequence {
        return if (position == 0) {
            context.getString(R.string.food_list)
        } else {
            context.getString(R.string.ration)
        }

    }
}