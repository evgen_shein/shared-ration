package viked.geneticalgorithm.view.adapters

import android.graphics.Color
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.meal_row.view.*
import viked.geneticalgorithm.R
import viked.geneticalgorithm.model.genetic.algorithm.objects.Meal
import viked.geneticalgorithm.view.objects.RationView
import kotlin.properties.Delegates

/**
 * Created by Viked on 7/23/2016.
 */
class MealListAdapter : RecyclerView.Adapter<MealListAdapter.MealViewHolder>() {

    var ration: RationView by Delegates.notNull<RationView>()

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
    }

    override fun getItemCount(): Int = ration.ration.mealRealmList.size

    override fun onBindViewHolder(holder: MealViewHolder, position: Int) {
        holder.bindView(position, ration.ration.mealRealmList[position]!!, position == ration.selected)}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MealViewHolder {
        return MealViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.meal_row, parent, false))
    }


    inner class MealViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val portionAdapter = PortionListAdapter()

        init {
            itemView.portions_list.adapter = portionAdapter
            itemView.meal_expand_check_box.setOnCheckedChangeListener { compoundButton, b ->
                ration.selected = if( b ) compoundButton.tag as Int else -1
                portionAdapter.setExpand(b)
                notifyDataSetChanged()
            }
        }

        fun bindView(position: Int, meal: Meal, isExpand: Boolean) {
            portionAdapter.updatePortion(meal, isExpand)
            itemView.meal_expand_check_box.tag = position
            itemView.meal_number.text = (position + 1).toString()
            if (isExpand) {
                itemView.setBackgroundResource(R.drawable.meal_item_background)
            } else {
                itemView.setBackgroundColor(Color.TRANSPARENT)
            }
        }
    }

}