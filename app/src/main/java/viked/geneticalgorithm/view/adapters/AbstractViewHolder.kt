package viked.geneticalgorithm.view.adapters

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by Viked on 8/21/2016.
 */
abstract class AbstractViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    abstract fun bindView(position: Int)
}