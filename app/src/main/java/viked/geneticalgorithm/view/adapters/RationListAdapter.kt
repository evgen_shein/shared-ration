package viked.geneticalgorithm.view.adapters


import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.ration_list_row.view.*
import viked.geneticalgorithm.R
import viked.geneticalgorithm.model.genetic.algorithm.objects.Ration
import viked.geneticalgorithm.view.objects.RationView


/**
 * Created by 1 on 01.08.2015.
 */
class RationListAdapter(val onClick: (Ration) -> Unit) : RecyclerView.Adapter<RationListAdapter.ViewHolder>() {

    private var list: List<RationView> = emptyList()

    fun getSelectionArray(): IntArray {
        return IntArray(list.size, { list[it].selected })
    }

    fun updateList(list: List<Ration>, selected: IntArray?) {
        this.list = when {
            this.list.size == list.size -> list.mapIndexed { i, ration -> RationView(ration, this.list[i].selected) }
            selected != null && selected.size == list.size -> list.mapIndexed { i, ration -> RationView(ration, selected[i]) }
            else -> list.map { RationView(it) }
        }
        notifyDataSetChanged()
    }


    override fun getItemCount() = list.size

    override fun onBindViewHolder(p0: ViewHolder, position: Int) {
        p0.bindData(list[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.ration_list_row, parent, false)
        val viewHolder = ViewHolder(view)
        return viewHolder
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val mealsAdapter: MealListAdapter = MealListAdapter()

        init {
            itemView.row_text_grid.adapter = mealsAdapter
            itemView.row_share.setOnClickListener { onClick(it.tag as Ration) }
        }

        fun bindData(data: RationView) {
            itemView.row_share.tag = data.ration
            itemView.row_title.text = "${itemView.context.getString(R.string.ration)} ${itemView.context.getString(R.string.number_unit)} ${data.ration.id}"
            itemView.row_title_total.text = itemView.context.getString(R.string.total_calories_args, data.ration.values.last().toInt())
            mealsAdapter.ration = data
            mealsAdapter.notifyDataSetChanged()
        }

    }

}