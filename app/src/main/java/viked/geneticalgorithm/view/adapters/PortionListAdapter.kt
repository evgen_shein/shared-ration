package viked.geneticalgorithm.view.adapters

import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.portion_row.view.*
import viked.geneticalgorithm.R
import viked.geneticalgorithm.model.genetic.algorithm.objects.Meal
import kotlin.properties.Delegates

/**
 * Created by Viked on 7/23/2016.
 */
class PortionListAdapter : RecyclerView.Adapter<PortionListAdapter.PortionViewHolder>() {

    private val stringRes = arrayOf(
            R.string.total_proteins,
            R.string.total_fats,
            R.string.total_carbohydrates,
            R.string.calorific
    )

    private var meal: Meal by Delegates.notNull<Meal>()

    private var values: Array<DoubleArray> by Delegates.notNull<Array<DoubleArray>>()

    private var valuesWeighted: Array<DoubleArray> by Delegates.notNull<Array<DoubleArray>>()

    private var expand: Boolean = false

    fun updatePortion(meal: Meal, expand: Boolean) {
        this.meal = meal
        values = Array(meal.portionRealmList.size, { i -> meal.portionRealmList[i]!!.values })
        valuesWeighted = Array(meal.portionRealmList.size, { i -> meal.portionRealmList[i]!!.getValues(100.0) })
        setExpand(expand)
    }

    fun setExpand(expand: Boolean) {
        this.expand = expand
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = if (expand) meal.portionRealmList.size * 5 else meal.portionRealmList.size


    override fun onBindViewHolder(holder: PortionViewHolder, position: Int) {
        if (expand) {
            val x = position / 5
            val y = position % 5

            if (y == 0) {
                holder.itemView.portion_title.text = meal.portionRealmList[x]!!.dish.name[0].name
                holder.itemView.portion_title.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorPrimaryText))
                holder.itemView.portion_data_first.text = holder.itemView.context.getString(R.string.per_100)
                holder.itemView.portion_data_second.text = holder.itemView.context.getString(R.string.per_100_args, meal.portionRealmList[x]!!.weight)
            } else {
                holder.itemView.portion_title.text = holder.itemView.context.getString(stringRes[y - 1])
                holder.itemView.portion_title.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorSecondaryText))
                holder.itemView.portion_data_first.text = String.format("%.2f", valuesWeighted[x][y - 1])
                holder.itemView.portion_data_second.text = String.format("%.2f", values[x][y - 1])
            }

            holder.itemView.divider.visibility = if (y != 4) View.GONE else View.VISIBLE

        } else {
            holder.itemView.portion_title.text = meal.portionRealmList[position]!!.dish.name[0].name
            holder.itemView.portion_title.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorPrimaryText))
            holder.itemView.portion_data_first.text = holder.itemView.context.getString(R.string.weight_unit_args, meal.portionRealmList[position]!!.weight)
            holder.itemView.portion_data_second.text = holder.itemView.context.getString(R.string.calories_unit_value_args, values[position].last())
            holder.itemView.divider.visibility = View.VISIBLE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PortionViewHolder {
        return PortionViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.portion_row, parent, false))
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
    }

    class PortionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}