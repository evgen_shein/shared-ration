package viked.geneticalgorithm.view.adapters

import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dialog_info_list_item.view.*
import viked.geneticalgorithm.R
import viked.geneticalgorithm.view.dialogs.InfoItem

/**
 * Created by Viked on 8/24/2016.
 */
class InfoListAdapter(val list: List<InfoItem>) : RecyclerView.Adapter<AbstractViewHolder>() {

    override fun getItemCount(): Int = list.count()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        return InfoItemViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.dialog_info_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: AbstractViewHolder, position: Int) {
        holder?.bindView(position)
    }

    inner class InfoItemViewHolder(view: View) : AbstractViewHolder(view) {

        init {
            view.dialog_info_list_item_content.movementMethod = LinkMovementMethod.getInstance()
        }

        override fun bindView(position: Int) {
            itemView.dialog_info_list_item_title.text = list[position].title
            itemView.dialog_info_list_item_content.text = Html.fromHtml(list[position].content)
        }

    }
}