package viked.geneticalgorithm.view.adapters

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.NativeExpressAdView
import viked.geneticalgorithm.R
import kotlin.properties.Delegates

/**
 * Created by Viked on 8/21/2016.
 */
abstract class BaseAdAdapter(val period: Int) : RecyclerView.Adapter<AbstractViewHolder>() {

    private val TYPE_ITEM = 0

    private val TYPE_AD = 1

    private var request: AdRequest by Delegates.notNull<AdRequest>()

    abstract fun getCount(): Int

    abstract fun onCreateItemViewHolder(parent: ViewGroup?): AbstractViewHolder

    override fun onBindViewHolder(holder: AbstractViewHolder, position: Int) {
        holder.bindView(position)
    }

    override fun getItemCount(): Int {
        return if (period > 0) {
            val count = getCount()
            var adCount = count / period
            if (adCount > 1) {
                adCount--
            }
            count + adCount
        } else {
            getCount()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        return if (viewType == TYPE_AD) {
            val adView = NativeExpressAdView(parent?.context)
            adView.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            adView.adSize = AdSize(AdSize.FULL_WIDTH, 100)
            adView.adUnitId = parent?.context?.getString(R.string.native_ad_unit_id)
            AdViewHolder(adView)
        } else {
            onCreateItemViewHolder(parent)
        }

    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        request = AdRequest.Builder()
                .addTestDevice("52A44BD6719640C7A760CBB2A98E10EE")
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build()

    }

    override fun getItemViewType(position: Int): Int {
        return if (period == 0 || position == 0 || position % period > 0) TYPE_ITEM else TYPE_AD

    }

    inner class AdViewHolder(val view: NativeExpressAdView) : AbstractViewHolder(view) {
        override fun bindView(position: Int) {
            view.loadAd(request)
        }
    }
}