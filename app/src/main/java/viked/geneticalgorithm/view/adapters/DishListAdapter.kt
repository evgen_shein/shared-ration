package viked.geneticalgorithm.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import kotlinx.android.synthetic.main.dish_list_row.view.*
import viked.geneticalgorithm.R
import viked.geneticalgorithm.model.data.containers.DishContainer
import viked.geneticalgorithm.model.getDishDescriptionList
import viked.geneticalgorithm.presenter.startSearch


/**
 * Created by 1 on 26.06.2016.
 */
class DishListAdapter(period: Int, val callback: Callback) : BaseAdAdapter(period) {

    interface Callback {
        fun setChecked(check: Boolean, container: DishContainer)
        fun setCheckedAll(check: Boolean)
        fun requestSearch(container: DishContainer)
    }

    private val dishes: MutableList<DishContainer> = mutableListOf()

    fun update(dishes: List<DishContainer>) {
        this.dishes.clear()
        this.dishes.addAll(dishes)
        notifyDataSetChanged()
    }

    override fun onCreateItemViewHolder(parent: ViewGroup?): AbstractViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.dish_list_row, parent, false)
        val viewHolder = ViewHolder(view)

        viewHolder.itemView.setOnClickListener { view ->
            val checkBox = view.row_check
            checkBox.isChecked = checkBox.isChecked.not()
        }
        viewHolder.itemView.row_check.setOnLongClickListener { view ->
            val checked = view as CheckBox
            callback.setCheckedAll(checked.isChecked.not())
            true
        }

        viewHolder.itemView.row_search_button.setOnClickListener { view ->
            val checkBox = (view.parent as View).row_check
            callback.requestSearch(dishes[checkBox.tag as Int])
        }
        return viewHolder
    }

    override fun getCount(): Int = dishes.size

    inner class ViewHolder(view: View) : AbstractViewHolder(view), CompoundButton.OnCheckedChangeListener {

        private val gridAdapter: GridAdapter = GridAdapter(2)

        init {
            itemView.row_text_grid.adapter = gridAdapter
        }

        override fun bindView(position: Int) {
            itemView.row_check.tag = position
            bindData(dishes[position])
        }

        private fun bindData(dishContainer: DishContainer) {
            itemView.dish_name.text = dishContainer.dish.name.first().name
            gridAdapter.list.clear()
            gridAdapter.list.addAll(getDishDescriptionList(itemView.context, dishContainer.dish))
            gridAdapter.notifyDataSetChanged()

            itemView.row_check.setOnCheckedChangeListener(null)
            itemView.row_check.isChecked = dishContainer.check
            itemView.row_check.setOnCheckedChangeListener(this)
        }

        override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
            val index = p0?.tag as Int
            callback.setChecked(p1, dishes[index])
        }

    }

}