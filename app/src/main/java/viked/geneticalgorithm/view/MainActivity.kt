package viked.geneticalgorithm.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewTreeObserver
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.squareup.picasso.Picasso
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKCallback
import com.vk.sdk.VKSdk
import com.vk.sdk.api.VKError
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import viked.geneticalgorithm.MyApplication
import viked.geneticalgorithm.R
import viked.geneticalgorithm.di.view.activity.ActivityComponent
import viked.geneticalgorithm.di.view.activity.ActivityModule
import viked.geneticalgorithm.presenter.IPresenter
import viked.geneticalgorithm.presenter.Presenter
import viked.geneticalgorithm.utils.CircleTransform
import viked.geneticalgorithm.view.dialogs.InfoDialog
import viked.geneticalgorithm.view.fragments.preference.PreferencesFragment
import viked.geneticalgorithm.view.fragments.tabs.ITabView
import viked.geneticalgorithm.view.fragments.tabs.TabsFragment
import javax.inject.Inject
import kotlin.properties.Delegates

class MainActivity : AppCompatActivity(), IView, NavigationView.OnNavigationItemSelectedListener, VKCallback<VKAccessToken> {

    private val DIALOG_INFO_TAG = "info_dialog"
    private val ACTION_DURATION = 5000

    private var mInterstitialAdShowed = false

    @Inject
    lateinit var presenter: IPresenter

    var component: ActivityComponent by Delegates.notNull<ActivityComponent>()

    private var mInterstitialAd: InterstitialAd  by Delegates.notNull<InterstitialAd>()

    private var searchView: SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        component = MyApplication.graph.plusActivityComponent(ActivityModule(this))
        component.inject(this)

        fab.setOnClickListener {
            presenter.fabAction()
            if (!mInterstitialAdShowed && mInterstitialAd.isLoaded) {
                mInterstitialAd.show()
            }
        }
        presenter.onCreate()

        if (savedInstanceState == null) {
            onSelectListFragment(0)
            onPageSelected(0)
        }

        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = getString(R.string.interstitial_ad_unit_id)

        mInterstitialAd.adListener = object : AdListener() {
            override fun onAdClosed() {
                mInterstitialAdShowed = true
            }
        }

        requestNewInterstitial()

        nav_view.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                if (nav_view.getHeaderView(0) != null) {
                    presenter.updateView()
                    nav_view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.activity_main_action_bar, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val searchItem = menu?.findItem(R.id.action_search)

        if (searchItem != null) {
            searchView = searchItem.actionView as SearchView?
            searchView?.setOnQueryTextListener(onQueryTextListener)
            searchView?.viewTreeObserver?.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    if (searchView != null) {
                        presenter.updateView()
                        searchView?.viewTreeObserver?.removeOnGlobalLayoutListener(this)
                    }
                }
            })
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun setFabVisibility(visibility: Boolean) {
        fab.visibility = if (visibility) View.VISIBLE else View.GONE
    }

    override fun setFabIcon(iconId: Int) {
        fab.setImageDrawable(ContextCompat.getDrawable(this, iconId))
    }

    override fun showMassage(massageErrorId: Int) {
        Snackbar.make(fab, massageErrorId, Snackbar.LENGTH_LONG).show()
    }

    override fun getContext(): Context = this

    override fun inject(presenter: IPresenter) {
        component.inject(presenter as Presenter)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_food_list -> onSelectListFragment(FOOD_LIST_FRAGMENT_INDEX)
            R.id.nav_result_list -> onSelectListFragment(RESULT_LIST_FRAGMENT_INDEX)
            R.id.nav_settings -> onSelectSettingsFragment()
            R.id.nav_sent_to_developer -> presenter.contactByEmail()
            R.id.nav_login_vk -> presenter.onLogin()
            R.id.nav_rate -> presenter.rate()
            R.id.nav_info -> showInfoDialog()
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun showInfoDialog() {
        InfoDialog().show(supportFragmentManager, DIALOG_INFO_TAG)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onPageSelected(position: Int) {
        nav_view.setCheckedItem(if (position == FOOD_LIST_FRAGMENT_INDEX) R.id.nav_food_list else R.id.nav_result_list)
        presenter.updateView()
    }

    override fun getCurrentFragment(): Int {
        val fm = supportFragmentManager
        val fragment = fm.findFragmentById(R.id.frameLayout)
        return if (fragment is PreferencesFragment) {
            PREFERENCE_FRAGMENT_INDEX
        } else {
            (fragment as ITabView?)?.getSelectedTab() ?: FOOD_LIST_FRAGMENT_INDEX
        }
    }

    private fun requestNewInterstitial() {
        val adRequest = AdRequest.Builder()
                .addTestDevice("52A44BD6719640C7A760CBB2A98E10EE")
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build()

        mInterstitialAd.loadAd(adRequest)
    }

    private fun onSelectSettingsFragment() {
        onClearFragmentManager()
        val fragment = PreferencesFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frameLayout, fragment, fragment.javaClass.canonicalName)
        fragmentTransaction.commit()
        presenter.updateView()
    }

    override fun setSearchVisibility(visibility: Boolean) {
        searchView?.visibility = if (visibility) View.VISIBLE else View.GONE
    }

    override fun onSelectListFragment(position: Int) {
        val tabFragment = supportFragmentManager.findFragmentByTag(TabsFragment.TAG) as ITabView?
        if (tabFragment != null) {
            tabFragment.onSelectTab(position)
        } else {
            val fragment = TabsFragment.newInstance(position)
            supportFragmentManager.beginTransaction()
                    .replace(R.id.frameLayout, fragment, TabsFragment.TAG)
                    .commit()
        }
        presenter.updateView()
    }

    private fun onClearFragmentManager() {
        val fm = supportFragmentManager
        val transaction = fm.beginTransaction()
        fm.fragments?.filter { it != null }?.forEach { transaction.remove(it) }
        transaction.commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, this)) {
            super.onActivityResult(requestCode, resultCode, data)
        } else {
            (application as MyApplication).initUser()
            presenter.requestJoinGroupIfNeed()
        }
    }

    override fun onError(error: VKError?) {
        onMassage(R.string.login_error)
    }

    override fun onResult(res: VKAccessToken?) {

    }

    override fun onMassage(stringId: Int) {
        Snackbar.make(frameLayout, stringId, Snackbar.LENGTH_LONG).show()
    }

    override fun requestAction(messageStringId: Int, actionStringId: Int, action: () -> Unit) {
        Snackbar.make(frameLayout, messageStringId, Snackbar.LENGTH_INDEFINITE)
                .setDuration(ACTION_DURATION)
                .setAction(actionStringId, { action.invoke() }).show()
    }

    override fun setUserName(name: String) {
        if (nav_view.nav_header_text_view != null) {
            nav_view.nav_header_text_view.text = name
        }
    }

    override fun setUserIconId(iconId: Int) {
        if (nav_view.nav_header_image_view != null) {
            nav_view.nav_header_image_view.setImageResource(iconId)
        }
    }

    override fun setUserIcon(imageUrl: String) {
        if (nav_view.nav_header_image_view != null) {
            Picasso.with(this).load(imageUrl).transform(CircleTransform()).into(nav_view.nav_header_image_view)
        }
    }

    override fun setLoginButtonText(textId: Int) {
        nav_view.menu.findItem(R.id.nav_login_vk).setTitle(textId)
    }

    val onQueryTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            searchView?.clearFocus()
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            presenter.requestSearch(newText ?: "")
            return true
        }
    }
}
