package viked.geneticalgorithm

import android.app.Application
import com.google.firebase.database.FirebaseDatabase
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKAccessTokenTracker
import com.vk.sdk.VKSdk
import io.realm.Realm
import io.realm.RealmConfiguration
import rx.subjects.PublishSubject
import viked.geneticalgorithm.di.ApplicationComponent
import viked.geneticalgorithm.di.DaggerApplicationComponent
import viked.geneticalgorithm.di.ModelModule
import viked.geneticalgorithm.model.data.objects.User
import viked.geneticalgorithm.model.vk.VKModel
import viked.geneticalgorithm.state.IApplicationState
import viked.geneticalgorithm.state.UpdateViewState
import viked.geneticalgorithm.state.VKState
import javax.inject.Inject

/**
 * Created by 1 on 22.03.2016.
 */
class MyApplication : Application() {

    companion object {
        //platformStatic allow access it from java code
        @JvmStatic
        lateinit var graph: ApplicationComponent
    }

    @Inject
    lateinit var stateSubject: PublishSubject<IApplicationState>

    @Inject
    lateinit var vkModel: VKModel

    val tracker = VKTokenTracker()

    override fun onCreate() {
        super.onCreate()
        FirebaseDatabase.getInstance().setPersistenceEnabled(true)

        tracker.startTracking()
        VKSdk.initialize(this)
        Realm.init(this)
        val realmConfig = RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded() //TODO add migration
                .build()
        Realm.setDefaultConfiguration(realmConfig)
        initGraph()

        initUser()
    }

    override fun onTerminate() {
        tracker.stopTracking()
        super.onTerminate()
    }

    fun initGraph() {
        graph = DaggerApplicationComponent.builder()
                .modelModule(ModelModule(this))
                .build()
        graph.inject(this)
    }

    fun initUser() {
        if (VKAccessToken.currentToken() != null) {
            vkModel.getUser().subscribe(
                    {
                        User.createUser(Realm.getDefaultInstance(), it)
                        stateSubject.onNext(VKState(VKState.RequestType.LOGIN))
                    },
                    {
                        it.printStackTrace()
                        User.clearUser(Realm.getDefaultInstance())
                        stateSubject.onNext(UpdateViewState())
                    }
            )
        } else {
            User.clearUser(Realm.getDefaultInstance())
            stateSubject.onNext(UpdateViewState())
        }
    }


    inner class VKTokenTracker : VKAccessTokenTracker() {
        override fun onVKAccessTokenChanged(oldToken: VKAccessToken?, newToken: VKAccessToken?) {
            if (newToken == null) {
                stateSubject.onNext(VKState(VKState.RequestType.LOGIN))
            }
        }
    }

}