package viked.geneticalgorithm.di

import javax.inject.Qualifier

/**
 * Created by 1 on 01.03.2016.
 */

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ForApplication
