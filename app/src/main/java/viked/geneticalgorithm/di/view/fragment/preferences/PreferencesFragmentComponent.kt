package viked.geneticalgorithm.di.view.fragment.preferences

import dagger.Subcomponent
import viked.geneticalgorithm.di.view.fragment.ForFragment
import viked.geneticalgorithm.presenter.fragments.preferences.IPreferencePresenter
import viked.geneticalgorithm.presenter.fragments.preferences.PreferencesPresenterImpl
import viked.geneticalgorithm.view.fragments.preference.PreferencesFragment

/**
 * Created by 1 on 29.03.2016.
 */
@ForFragment
@Subcomponent(modules = arrayOf(PreferencesFragmentModule::class))
interface PreferencesFragmentComponent {

    fun inject(preferencesPresenter: PreferencesPresenterImpl)

    fun inject(preferenceFragment: PreferencesFragment)

}