package viked.geneticalgorithm.di.view.fragment.tabs.food

import dagger.Module
import dagger.Provides
import viked.geneticalgorithm.di.view.fragment.ForFragment
import viked.geneticalgorithm.di.view.fragment.tabs.ForTabFragment
import viked.geneticalgorithm.presenter.fragments.food.FoodPresenterImpl
import viked.geneticalgorithm.presenter.fragments.food.IFoodPresenter
import viked.geneticalgorithm.view.fragments.food.list.IFoodListView

/**
 * Created by Viked on 7/28/2016.
 */
@Module
class FoodFragmentModule(val view: IFoodListView) {

    @ForTabFragment
    @Provides
    fun providePresenter(): IFoodPresenter {
        return FoodPresenterImpl(view)
    }
}