package viked.geneticalgorithm.di.view.fragment.tabs

import dagger.Module
import dagger.Provides
import viked.geneticalgorithm.di.view.fragment.ForFragment
import viked.geneticalgorithm.presenter.fragments.tabs.ITabPresenter
import viked.geneticalgorithm.presenter.fragments.tabs.TabPresenterImpl
import viked.geneticalgorithm.view.fragments.tabs.ITabView

/**
 * Created by Viked on 7/28/2016.
 */
@Module
class TabFragmentModule(val view: ITabView) {

    @ForFragment
    @Provides
    fun providePresenter(): ITabPresenter {
        return TabPresenterImpl(view)
    }

}