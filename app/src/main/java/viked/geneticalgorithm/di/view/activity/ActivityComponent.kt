package viked.geneticalgorithm.di.view.activity

import dagger.Subcomponent
import viked.geneticalgorithm.di.view.fragment.preferences.PreferencesFragmentComponent
import viked.geneticalgorithm.di.view.fragment.preferences.PreferencesFragmentModule
import viked.geneticalgorithm.di.view.fragment.tabs.TabFragmentComponent
import viked.geneticalgorithm.di.view.fragment.tabs.TabFragmentModule
import viked.geneticalgorithm.presenter.Presenter
import viked.geneticalgorithm.view.IView
import viked.geneticalgorithm.view.MainActivity

/**
 * Created by 1 on 29.03.2016.
 */
@ForActivity
@Subcomponent(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {

    fun plusPreferencesFragmentComponent(preferencesFragmentModule: PreferencesFragmentModule): PreferencesFragmentComponent

    fun plusTabFragmentComponent(module: TabFragmentModule): TabFragmentComponent

    fun inject(mainActivity: MainActivity)

    fun inject(presenter: Presenter)

    fun getView(): IView

}