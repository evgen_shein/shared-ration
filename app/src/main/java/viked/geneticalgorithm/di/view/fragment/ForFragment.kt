package viked.geneticalgorithm.di.view.fragment

import javax.inject.Scope

/**
 * Created by Viked on 7/28/2016.
 */

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ForFragment