package viked.geneticalgorithm.di.view.fragment.tabs.result

import dagger.Subcomponent
import viked.geneticalgorithm.di.view.fragment.tabs.ForTabFragment
import viked.geneticalgorithm.presenter.fragments.result.ResultPresenterImpl
import viked.geneticalgorithm.view.fragments.result.ration.ResultRationFragment

/**
 * Created by 1 on 29.03.2016.
 */
@ForTabFragment
@Subcomponent(modules = arrayOf(ResultFragmentModule::class))
interface ResultFragmentComponent {

    fun inject(presenter: ResultPresenterImpl)

    fun inject(fragment: ResultRationFragment)

}