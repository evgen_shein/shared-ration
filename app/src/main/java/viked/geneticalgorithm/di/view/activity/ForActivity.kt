package viked.geneticalgorithm.di.view.activity

import javax.inject.Scope

/**
 * Created by 1 on 29.03.2016.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ForActivity