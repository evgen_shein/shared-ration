package viked.geneticalgorithm.di.view.fragment.tabs.result

import dagger.Module
import dagger.Provides
import viked.geneticalgorithm.di.view.fragment.ForFragment
import viked.geneticalgorithm.di.view.fragment.tabs.ForTabFragment
import viked.geneticalgorithm.presenter.fragments.food.FoodPresenterImpl
import viked.geneticalgorithm.presenter.fragments.food.IFoodPresenter
import viked.geneticalgorithm.presenter.fragments.result.IResultPresenter
import viked.geneticalgorithm.presenter.fragments.result.ResultPresenterImpl
import viked.geneticalgorithm.view.fragments.food.list.IFoodListView
import viked.geneticalgorithm.view.fragments.result.ration.IResultRationView

/**
 * Created by Viked on 7/28/2016.
 */
@Module
class ResultFragmentModule(val view: IResultRationView) {

    @ForTabFragment
    @Provides
    fun providePresenter(): IResultPresenter {
        return ResultPresenterImpl(view)
    }
}