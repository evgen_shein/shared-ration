package viked.geneticalgorithm.di.view.fragment.tabs.food

import dagger.Subcomponent
import viked.geneticalgorithm.di.view.fragment.tabs.ForTabFragment
import viked.geneticalgorithm.presenter.fragments.food.FoodPresenterImpl
import viked.geneticalgorithm.presenter.fragments.food.IFoodPresenter
import viked.geneticalgorithm.view.fragments.food.list.FoodListFragment

/**
 * Created by 1 on 29.03.2016.
 */
@ForTabFragment
@Subcomponent(modules = arrayOf(FoodFragmentModule::class))
interface FoodFragmentComponent {

    fun inject(presenter: FoodPresenterImpl)

    fun inject(fragment: FoodListFragment)

}