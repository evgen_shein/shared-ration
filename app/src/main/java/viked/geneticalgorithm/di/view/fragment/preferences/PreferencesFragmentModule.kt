package viked.geneticalgorithm.di.view.fragment.preferences

import dagger.Module
import dagger.Provides
import viked.geneticalgorithm.di.view.fragment.ForFragment
import viked.geneticalgorithm.presenter.fragments.preferences.IPreferencePresenter
import viked.geneticalgorithm.presenter.fragments.preferences.PreferencesPresenterImpl
import viked.geneticalgorithm.view.fragments.preference.IPreferencesView

/**
 * Created by Viked on 7/28/2016.
 */
@Module
class PreferencesFragmentModule(val fragment: IPreferencesView) {

    @ForFragment
    @Provides
    fun providePresenter(): IPreferencePresenter {
        return PreferencesPresenterImpl(fragment)
    }


}