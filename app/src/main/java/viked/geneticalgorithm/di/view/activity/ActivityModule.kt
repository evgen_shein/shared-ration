package viked.geneticalgorithm.di.view.activity

import dagger.Module
import dagger.Provides
import viked.geneticalgorithm.di.view.activity.ForActivity
import viked.geneticalgorithm.presenter.IPresenter
import viked.geneticalgorithm.presenter.Presenter
import viked.geneticalgorithm.view.IView
import viked.geneticalgorithm.view.fragments.food.list.IFoodListView
import viked.geneticalgorithm.view.fragments.preference.IPreferencesView
import viked.geneticalgorithm.view.fragments.result.ration.IResultRationView

/**
 * Created by 1 on 29.03.2016.
 */
@Module
class ActivityModule(val view: IView) {

    @ForActivity
    @Provides
    fun providePresenter(): IPresenter {
        return Presenter(view)
    }

    @ForActivity
    @Provides
    fun provideView(): IView {
        return view
    }

}