package viked.geneticalgorithm.di.view.fragment.tabs

import dagger.Subcomponent
import viked.geneticalgorithm.di.view.fragment.ForFragment
import viked.geneticalgorithm.di.view.fragment.tabs.food.FoodFragmentComponent
import viked.geneticalgorithm.di.view.fragment.tabs.food.FoodFragmentModule
import viked.geneticalgorithm.di.view.fragment.tabs.result.ResultFragmentComponent
import viked.geneticalgorithm.di.view.fragment.tabs.result.ResultFragmentModule
import viked.geneticalgorithm.presenter.fragments.tabs.TabPresenterImpl
import viked.geneticalgorithm.view.fragments.tabs.TabsFragment

/**
 * Created by 1 on 29.03.2016.
 */
@ForFragment
@Subcomponent(modules = arrayOf(TabFragmentModule::class))
interface TabFragmentComponent {

    fun plusFoodFragmentComponent(module: FoodFragmentModule): FoodFragmentComponent

    fun plusResultFragmentComponent(module: ResultFragmentModule): ResultFragmentComponent

    fun inject(presenter: TabPresenterImpl)

    fun inject(tabFragment: TabsFragment)

}