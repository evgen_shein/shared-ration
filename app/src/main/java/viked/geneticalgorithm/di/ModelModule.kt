package viked.geneticalgorithm.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import rx.subjects.PublishSubject
import viked.geneticalgorithm.model.data.DataBaseModel
import viked.geneticalgorithm.model.genetic.algorithm.GeneticAlgorithmModel
import viked.geneticalgorithm.model.preferences.IPreferencesModel
import viked.geneticalgorithm.model.preferences.PreferencesModelImpl
import viked.geneticalgorithm.model.vk.VKModel
import viked.geneticalgorithm.state.IApplicationState
import javax.inject.Singleton

/**
 * Created by 1 on 26.03.2016.
 */
@Module
class ModelModule(val application: Application) {

    @Singleton
    @Provides
    fun provideDataBaseModel(): DataBaseModel {
        return DataBaseModel(application)
    }

    @Singleton
    @Provides
    fun provideGeneticAlgorithmModel(): GeneticAlgorithmModel {
        return GeneticAlgorithmModel()
    }

    @Singleton
    @Provides
    fun provideApplicationContext(): Context {
        return application
    }

    @Singleton
    @Provides
    fun provideApplication(): Application {
        return application
    }

    @Singleton
    @Provides
    fun providePreferencesModel(): IPreferencesModel {
        val preferencesModel = PreferencesModelImpl(application)
        preferencesModel.loadPreferences()
        return preferencesModel
    }

    @Singleton
    @Provides
    fun provideVKModel(): VKModel {
        return VKModel()
    }

    @Singleton
    @Provides
    fun provideStateSubject(): PublishSubject<IApplicationState> {
        return PublishSubject.create()
    }

}