package viked.geneticalgorithm.di

import dagger.Component
import rx.subjects.PublishSubject
import viked.geneticalgorithm.MyApplication
import viked.geneticalgorithm.di.view.activity.ActivityComponent
import viked.geneticalgorithm.di.view.activity.ActivityModule
import viked.geneticalgorithm.model.data.DataBaseModel
import viked.geneticalgorithm.model.genetic.algorithm.GeneticAlgorithmModel
import viked.geneticalgorithm.model.preferences.IPreferencesModel
import viked.geneticalgorithm.model.preferences.PreferencesModelImpl
import viked.geneticalgorithm.model.vk.VKModel
import viked.geneticalgorithm.state.IApplicationState
import viked.geneticalgorithm.view.SplashActivity
import javax.inject.Singleton

/**
 * Created by 1 on 07.03.2016.
 */
@Singleton
@Component(modules = arrayOf(ModelModule::class))
interface ApplicationComponent {

    fun plusActivityComponent(activityModule: ActivityModule): ActivityComponent

    fun inject(myApplication: MyApplication)

    fun inject(geneticAlgorithmModel: GeneticAlgorithmModel)

    fun inject(dataBaseModel: DataBaseModel)

    fun inject(preferencesModel: PreferencesModelImpl)

    fun inject(splashActivity: SplashActivity)

    fun inject(vkModel: VKModel)

    fun getPreferencesModel(): IPreferencesModel

    fun getGeneticAlgorithmModel(): GeneticAlgorithmModel

    fun getDataBaseModel(): DataBaseModel

    fun getStateSubject(): PublishSubject<IApplicationState>

}