package viked.geneticalgorithm.presenter.fragments.tabs

import viked.geneticalgorithm.view.fragments.food.list.FoodListFragment
import viked.geneticalgorithm.view.fragments.food.list.IFoodListView
import viked.geneticalgorithm.view.fragments.result.ration.IResultRationView
import viked.geneticalgorithm.view.fragments.result.ration.ResultRationFragment
import viked.geneticalgorithm.view.fragments.tabs.ITabView

/**
 * Created by Viked on 7/30/2016.
 */
class TabPresenterImpl(val view: ITabView) : ITabPresenter {

    init {
        view.injectPresenter(this)
    }

    lateinit var foodListView: IFoodListView

    lateinit var resultListView: IResultRationView

    override fun getFoodFragment(): FoodListFragment {
        foodListView = FoodListFragment()
        return foodListView as FoodListFragment
    }

    override fun getResultFragment(): ResultRationFragment {
        resultListView = ResultRationFragment()
        return resultListView as ResultRationFragment
    }

}