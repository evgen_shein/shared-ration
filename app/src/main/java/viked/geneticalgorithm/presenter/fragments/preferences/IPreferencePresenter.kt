package viked.geneticalgorithm.presenter.fragments.preferences

import android.content.SharedPreferences
import android.support.v7.preference.PreferenceManager

/**
 * Created by Viked on 7/28/2016.
 */
interface IPreferencePresenter {
    fun updatePreference(sharedPreferences: SharedPreferences, key: String)
    fun restoreDefaultPreferences(preferenceManager: PreferenceManager)
}