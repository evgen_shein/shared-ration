package viked.geneticalgorithm.presenter.fragments.result

import io.realm.Realm
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subjects.PublishSubject
import viked.algorithmlibrary.data.IAlgorithmState
import viked.geneticalgorithm.R
import viked.geneticalgorithm.model.genetic.algorithm.GeneticAlgorithmModel
import viked.geneticalgorithm.model.genetic.algorithm.objects.Ration
import viked.geneticalgorithm.state.AlgorithmState
import viked.geneticalgorithm.state.IApplicationState
import viked.geneticalgorithm.state.UpdateViewState
import viked.geneticalgorithm.state.VKSendState
import viked.geneticalgorithm.view.fragments.result.ration.IResultRationView
import javax.inject.Inject

/**
 * Created by Viked on 7/30/2016.
 */
class ResultPresenterImpl(val view: IResultRationView) : IResultPresenter {

    @Inject
    override lateinit var eventSubject: PublishSubject<IApplicationState>

    @Inject
    lateinit var geneticAlgorithmModel: GeneticAlgorithmModel

    init {
        view.injectPresenter(this)
        eventSubject
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    when (it) {
                        is AlgorithmState -> updateView()
                        is UpdateViewState -> updateView()
                    }
                }, {
                    it.printStackTrace()
                })
    }

    override fun onShare(ration: Ration) {
        eventSubject.onNext(VKSendState(getMassage(ration)))
    }

    private fun getMassage(ration: Ration): String {
        val stringBuffer = StringBuffer()

        val values = ration.values
        stringBuffer.append(
                view.context()
                        .getString(R.string.share_ration_text,
                                values[values.lastIndex], values[0], values[1], values[2]))

        ration.mealRealmList.forEachIndexed { i, meal ->
            stringBuffer.append("\n")
            meal.portionRealmList.forEachIndexed { j, portion ->
                stringBuffer.append("${i + 1}.${j + 1} ${portion.dish.name[0].name} ${view.context().getString(R.string.weight_unit_args, portion.weight)}\n")
            }
        }

        return stringBuffer.toString()
    }

    override fun requestUpdate() {
        eventSubject.onNext(UpdateViewState())
    }

    override fun updateView() {
        view.updateList(Ration.getAll(Realm.getDefaultInstance()))
        val isRunning = geneticAlgorithmModel.isRunning()
        view.setProgressBarVisibility(isRunning)
        if (isRunning && subscriber == null) {
            subscriber = newSubscriber()
            geneticAlgorithmModel.run().subscribe(subscriber)
        }
    }

    override fun isRunning(): Boolean = subscriber != null

    private var subscriber: Subscriber<IAlgorithmState>? = null

    private fun newSubscriber(): Subscriber<IAlgorithmState> {
        return object : Subscriber<IAlgorithmState>() {

            override fun onCompleted() {
                subscriber = null
            }

            override fun onError(e: Throwable) {
                subscriber = null
            }

            override fun onNext(l: IAlgorithmState) {
                val progress = (l.iterationProgress * 100.0).toInt()
                view.updateProgress(progress,
                        view.context().getString(R.string.fragment_result_progress_massage, l.iterationNumber.toString(), progress.toString()))
            }
        }
    }
}