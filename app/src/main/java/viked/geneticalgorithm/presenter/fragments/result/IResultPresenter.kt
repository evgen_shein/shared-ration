package viked.geneticalgorithm.presenter.fragments.result

import viked.geneticalgorithm.model.genetic.algorithm.objects.Ration
import viked.geneticalgorithm.presenter.IBasePresenter

/**
 * Created by Viked on 7/30/2016.
 */
interface IResultPresenter : IBasePresenter {
    fun onShare(ration: Ration)
    fun isRunning(): Boolean
}