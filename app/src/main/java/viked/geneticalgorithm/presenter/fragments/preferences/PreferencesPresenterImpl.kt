package viked.geneticalgorithm.presenter.fragments.preferences

import android.content.SharedPreferences
import android.support.v7.preference.PreferenceManager
import viked.geneticalgorithm.model.preferences.IPreferencesModel
import viked.geneticalgorithm.model.preferences.custom.INumericalPreference
import viked.geneticalgorithm.view.fragments.preference.IPreferencesView
import javax.inject.Inject

/**
 * Created by Viked on 7/28/2016.
 */
class PreferencesPresenterImpl(preferencesFragment: IPreferencesView) : IPreferencePresenter {

    @Inject
    lateinit var preferencesModel: IPreferencesModel

    init {
        preferencesFragment.injectPresenter(this)
    }

    override fun restoreDefaultPreferences(preferenceManager: PreferenceManager) {
        preferencesModel.getDefaultPreferences().forEach { e ->
            (preferenceManager.findPreference(e.key)!! as INumericalPreference).value = e.value
        }
    }

    override fun updatePreference(sharedPreferences: SharedPreferences, key: String) {
        val defaultValue = preferencesModel.getDefaultPreferences()[key]!!
        preferencesModel.updatePreference(key, sharedPreferences.getInt(key, defaultValue))
    }

}