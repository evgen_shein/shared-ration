package viked.geneticalgorithm.presenter.fragments.food

import viked.geneticalgorithm.presenter.IBasePresenter
import viked.geneticalgorithm.view.adapters.DishListAdapter

/**
 * Created by Viked on 7/30/2016.
 */
interface IFoodPresenter : IBasePresenter, DishListAdapter.Callback