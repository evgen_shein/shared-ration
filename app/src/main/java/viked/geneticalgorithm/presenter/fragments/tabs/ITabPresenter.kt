package viked.geneticalgorithm.presenter.fragments.tabs

import viked.geneticalgorithm.view.fragments.food.list.FoodListFragment
import viked.geneticalgorithm.view.fragments.result.ration.ResultRationFragment

/**
 * Created by Viked on 7/30/2016.
 */
interface ITabPresenter {

    fun getFoodFragment(): FoodListFragment

    fun getResultFragment(): ResultRationFragment

}