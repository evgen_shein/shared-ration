package viked.geneticalgorithm.presenter.fragments.food

import io.realm.Realm
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subjects.PublishSubject
import viked.geneticalgorithm.model.data.containers.DishContainer
import viked.geneticalgorithm.presenter.startSearch
import viked.geneticalgorithm.state.IApplicationState
import viked.geneticalgorithm.state.SearchState
import viked.geneticalgorithm.state.UpdateViewState
import viked.geneticalgorithm.view.fragments.food.list.IFoodListView
import javax.inject.Inject

/**
 * Created by Viked on 7/30/2016.
 */
class FoodPresenterImpl(val view: IFoodListView) : IFoodPresenter {

    @Inject
    override lateinit var eventSubject: PublishSubject<IApplicationState>

    private var searchString = ""

    init {
        view.injectPresenter(this)
        eventSubject
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    when (it) {
                        is SearchState -> {
                            searchString = it.text
                            updateView()
                        }
                        is UpdateViewState -> updateView()
                    }
                }, {
                    it.printStackTrace()
                })
    }


    override fun requestUpdate() {
        eventSubject.onNext(UpdateViewState())
    }

    override fun updateView() {
        view.updateList(DishContainer.getAll(Realm.getDefaultInstance()).filter { it.dish.name[0].name.contains(searchString) })
    }

    override fun setChecked(check: Boolean, container: DishContainer) {
        Realm.getDefaultInstance().executeTransaction {
            if (container.check != check) {
                container.check = check
            }
            requestUpdate()
        }
    }

    override fun setCheckedAll(check: Boolean) {
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            DishContainer.getAll(realm)
                    .filter { it.dish.name[0].name.contains(searchString) }
                    .forEach { it.check = check }
            requestUpdate()
        }
    }

    override fun requestSearch(container: DishContainer) {
        startSearch(view.context(), container.dish.name.first().name)
    }
}