package viked.geneticalgorithm.presenter

import rx.subjects.PublishSubject
import viked.geneticalgorithm.state.IApplicationState

/**
 * Created by Viked on 8/22/2016.
 */
interface IBasePresenter {
    var eventSubject: PublishSubject<IApplicationState>
    fun requestUpdate()
    fun updateView()
}