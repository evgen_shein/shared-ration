package viked.geneticalgorithm.presenter


/**
 * Created by 1 on 22.03.2016.
 */
interface IPresenter : IBasePresenter {
    fun onCreate()

    fun onDestroy()

    fun fabAction()

    fun onLogin()

    fun contactByEmail()

    fun requestSearch(text: String)

    fun rate()

    fun requestJoinGroupIfNeed()

}