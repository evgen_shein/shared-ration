package viked.geneticalgorithm.presenter

import android.content.Intent
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKScope
import com.vk.sdk.VKSdk
import com.vk.sdk.api.VKError
import com.vk.sdk.dialogs.VKShareDialog
import com.vk.sdk.dialogs.VKShareDialogBuilder
import io.realm.Realm
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subjects.PublishSubject
import rx.subscriptions.CompositeSubscription
import viked.algorithmlibrary.data.IAlgorithmState
import viked.geneticalgorithm.BuildConfig
import viked.geneticalgorithm.R
import viked.geneticalgorithm.model.data.objects.User
import viked.geneticalgorithm.model.genetic.algorithm.GeneticAlgorithmModel
import viked.geneticalgorithm.model.getAlgorithmEnable
import viked.geneticalgorithm.model.preferences.IPreferencesModel
import viked.geneticalgorithm.model.vk.VKModel
import viked.geneticalgorithm.state.*
import viked.geneticalgorithm.view.FOOD_LIST_FRAGMENT_INDEX
import viked.geneticalgorithm.view.IView
import viked.geneticalgorithm.view.MainActivity
import viked.geneticalgorithm.view.PREFERENCE_FRAGMENT_INDEX
import javax.inject.Inject

/**
 * Created by 1 on 26.03.2016.
 */
class Presenter(val view: IView) : IPresenter {

    private val RESULT_PAGE_INDEX = 1

    @Inject
    override lateinit var eventSubject: PublishSubject<IApplicationState>

    @Inject
    lateinit var preferencesModel: IPreferencesModel

    @Inject
    lateinit var geneticAlgorithmModel: GeneticAlgorithmModel

    @Inject
    lateinit var vkModel: VKModel

    private val subscriber: CompositeSubscription = CompositeSubscription()

    init {
        view.inject(this)
        subscriber.add(eventSubject
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    when (it) {
                        is VKState -> {
                            if (it.type == VKState.RequestType.LOGIN) {
                                requestJoinGroupIfNeed()
                            } else {
                                onLogin()
                            }
                            updateView()
                        }
                        is VKSendState -> onSendRation(it.massage)
                        is UpdateViewState -> updateView()
                    }
                }, {
                    it.printStackTrace()
                }))
    }

    private fun getAlgorithmSubscriber(): Subscriber<IAlgorithmState> {
        return object : Subscriber<IAlgorithmState>() {
            var iteration = 0

            override fun onCompleted() {
                requestUpdate()
            }

            override fun onError(e: Throwable) {
                e.printStackTrace()
                view.showMassage(R.string.error)
            }

            override fun onNext(l: IAlgorithmState) {
                if (iteration != l.iterationNumber) {
                    iteration = l.iterationNumber
                    requestUpdate()
                }
                if (l.complete)
                    requestUpdate()
            }
        }
    }

    override fun onCreate() {
        if (geneticAlgorithmModel.isRunning()) {
            geneticAlgorithmModel.run().subscribe(getAlgorithmSubscriber())
        }
    }

    override fun onDestroy() {
        subscriber.clear()
    }

    override fun requestUpdate() {
        eventSubject.onNext(UpdateViewState())
    }

    override fun fabAction() {
        if (!geneticAlgorithmModel.isRunning() || geneticAlgorithmModel.isComplete()) {
            geneticAlgorithmModel.run().subscribe(getAlgorithmSubscriber())
            view.onSelectListFragment(RESULT_PAGE_INDEX)
            view.onPageSelected(RESULT_PAGE_INDEX)
        } else {
            geneticAlgorithmModel.stop()
        }
        requestUpdate()
    }

    override fun onLogin() {
        if (VKAccessToken.currentToken() != null) {
            VKSdk.logout()
            User.clearUser(Realm.getDefaultInstance())
            requestUpdate()
        } else {
            VKSdk.login(view as MainActivity, VKScope.WALL, VKScope.GROUPS)
        }

    }

    override fun updateView() {
        val currentFragment = view.getCurrentFragment()
        val algorithmIsRunning = geneticAlgorithmModel.isRunning()
        view.setFabVisibility(currentFragment != PREFERENCE_FRAGMENT_INDEX && getAlgorithmEnable(Realm.getDefaultInstance()))
        view.setFabIcon(if (algorithmIsRunning) R.drawable.stop else R.drawable.play)
        view.setSearchVisibility(currentFragment == FOOD_LIST_FRAGMENT_INDEX)
        val isLogin = VKAccessToken.currentToken() != null

        if (isLogin) {
            val user = User.getUser(Realm.getDefaultInstance())
            if (user != null) {
                view.setUserName("${user.first_name} ${user.last_name}")
                view.setUserIcon(user.photo_200)
            }
            view.setLoginButtonText(R.string.navigation_drawer_title_logout)
        } else {
            //TODO add some massage
            view.setUserName("")
            view.setUserIconId(R.drawable.food_apple)
            view.setLoginButtonText(R.string.navigation_drawer_title_login)
        }


    }

    override fun contactByEmail() {
        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.type = "vnd.android.cursor.dir/email"
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, view.getContext().getString(R.string.app_name))
        emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(BuildConfig.DEVELOPER_EMAIL))
        view.getContext().startActivity(Intent.createChooser(emailIntent, "Send email..."))
    }

    override fun requestSearch(text: String) {
        eventSubject.onNext(SearchState(text))
    }

    private fun onSendRation(massage: String) {
        if (VKAccessToken.currentToken() != null) {
            val builder = VKShareDialogBuilder()
            builder.setText(massage)
            builder.setAttachmentLink(view.getContext().getString(R.string.app_name),
                    getLink(view.getContext()))
            builder.setShareDialogListener(object : VKShareDialog.VKShareDialogListener {
                override fun onVkShareComplete(postId: Int) {
                    view.onMassage(R.string.share_vk_complete)
                    requestUpdate()
                }

                override fun onVkShareCancel() {
                    // recycle bitmap if need
                }

                override fun onVkShareError(error: VKError) {
                    view.onMassage(R.string.share_vk_error)
                }
            })
            builder.show(view.getSupportFragmentManager(), "VK_SHARE_DIALOG")
        } else {
            view.requestAction(R.string.share_vk_login_request, R.string.navigation_drawer_title_login, { onLogin() })
        }
    }

    override fun rate() {
        goToMarket(view.getContext())
    }

    override fun requestJoinGroupIfNeed() {
        if (VKAccessToken.currentToken() != null) {
            val user = User.getUser(Realm.getDefaultInstance())
            if (user != null) {
                vkModel.checkUserGroup(user.id, BuildConfig.VK_GROUP_ID)
                        .subscribe({
                            if (!it) {
                                view.requestAction(R.string.vk_join_request, R.string.vk_join_button, { requestJoinGroup() })
                            }
                        }, {
                            it.printStackTrace()
                        })
            }
        }
    }

    private fun requestJoinGroup() {
        vkModel.joinGroup(BuildConfig.VK_GROUP_ID).subscribe({
            view.onMassage(if (it) R.string.vk_join_complete else R.string.share_vk_error)
        }, {
            it.printStackTrace()
            view.onMassage(R.string.share_vk_error)
        })
    }
}
