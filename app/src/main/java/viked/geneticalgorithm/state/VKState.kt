package viked.geneticalgorithm.state

/**
 * Created by Viked on 8/14/2016.
 */
class VKState(val type: RequestType) : IApplicationState {

    enum class RequestType {
        LOGIN,
        LOGOUT
    }
}