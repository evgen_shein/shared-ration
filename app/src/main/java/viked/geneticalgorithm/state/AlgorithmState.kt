package viked.geneticalgorithm.state

import viked.algorithmlibrary.data.IAlgorithmState
import viked.algorithmlibrary.data.Individual

/**
 * Created by Viked on 8/14/2016.
 */
class AlgorithmState(override val iterationProgress: Double, override val iterationNumber: Int, override val complete: Boolean) : IApplicationState, IAlgorithmState