package viked.geneticalgorithm.model

import android.content.Context
import io.realm.Realm
import viked.geneticalgorithm.R
import viked.geneticalgorithm.model.data.containers.DishContainer
import viked.geneticalgorithm.model.data.objects.Dish
import viked.geneticalgorithm.model.data.objects.WeightLimit
import viked.geneticalgorithm.model.genetic.algorithm.objects.Ration

/**
 * Created by 1 on 26.06.2016.
 */

fun getAlgorithmEnable(realm: Realm): Boolean {
    return DishContainer.getAll(realm).isNotEmpty()
}

fun getTypedDishList(realm: Realm): List<List<Dish>> {
    return DishContainer.getTypedList(realm).map { it.map { it.dish } }
}

fun getTypedWeightLimitList(realm: Realm): List<IntRange> {
    return WeightLimit.getTyped(realm).map { IntRange(it.min.toInt(), it.max.toInt()) }
}

fun getTotalCalories(dish: Dish, weight: Double): Int {
    return ((4.1 * getTotalProteins(dish, weight)) +
            (9.3 * getTotalFats(dish, weight)) +
            (4.1 * getTotalCarbohydrates(dish, weight))).toInt()
}

fun getTotalProteins(dish: Dish, weight: Double): Double {
    return dish.proteins / 100.0 * weight
}

fun getTotalFats(dish: Dish, weight: Double): Double {
    return dish.fats / 100.0 * weight
}

fun getTotalCarbohydrates(dish: Dish, weight: Double): Double {
    return dish.carbohydrates / 100.0 * weight
}

fun saveDailyRationList(realm: Realm, list: List<Ration>) {
    list.forEachIndexed { i, ration -> ration.id = (i + 1).toLong() }
    realm.executeTransaction {
        list.forEach { realm.copyToRealmOrUpdate(it) }
    }
}

fun getDishDescriptionList(context: Context, dish: Dish): List<String> {
    return listOf(
            context.getString(R.string.proteins),
            context.getString(R.string.weight_unit_value_args, dish.proteins),
            context.getString(R.string.fats),
            context.getString(R.string.weight_unit_value_args, dish.fats),
            context.getString(R.string.carbohydrates),
            context.getString(R.string.weight_unit_value_args, dish.carbohydrates),
            context.getString(R.string.total_calories_title),
            context.getString(R.string.total_calories_args, getTotalCalories(dish, 100.0)))
}







