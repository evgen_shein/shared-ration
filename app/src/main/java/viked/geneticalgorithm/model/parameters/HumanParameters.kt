package viked.geneticalgorithm.model.parameters

/**
 * Created by 1 on 26.03.2016.
 */
/*

    companion object{
        val MALE = 0
        val FEMALE = 1
    }

    /*
    val sex : Int
    val age : Int
    val weight : Int
    val height : Double
    */
    val energyConsumption = 2075



    var firstMealChance = 0.35
    var secondMealChance = 1.0
    var vegetablesChance = 0.30
    var fruitChance = 0.25
    var beverageschance = 0.9

    val mealChanceList : List<Double> = listOf(firstMealChance, secondMealChance, vegetablesChance, fruitChance, beverageschance)

    var proteinsEnergyDistribution = 0.11
    var fatsEnergyDistribution = 0.25
    var carbohydratesDistribution = 0.64


    var breakfastEnergyDistribution  = 0.30
    var lunchEnergyDistribution = 0.25
    var dinnerEnergyDistribution = 0.25
    var eveningMealEnergyDistribution = 0.20

    fun getActualDistribution(): List<Double> {
        return listOf(breakfastEnergyDistribution*energyConsumption,
                lunchEnergyDistribution*energyConsumption,
                dinnerEnergyDistribution*energyConsumption,
                eveningMealEnergyDistribution*energyConsumption)
    }

    /*





       val energyConsumption : Int = when(sex){
               MALE -> when(age){
                   in 10..18-> calcEnergyConsumption(16.6, 77.0, 572)
                   in 18..30-> calcEnergyConsumption(15.4, -27.0, 717)
                   in 30..60-> calcEnergyConsumption(11.3, 16.0, 901)
                   else-> calcEnergyConsumption(8.8, 1128.0, -1071)
               }

               FEMALE -> when(age){
                   in 10..18-> calcEnergyConsumption(7.4, 482.0, 217)
                   in 18..30-> calcEnergyConsumption(13.3, 334.0, 35)
                   in 30..60-> calcEnergyConsumption(8.7, -25.0, 865)
                   else-> calcEnergyConsumption(9.2, 637.0, -302)
               }
               else -> 0
           }


       private fun calcEnergyConsumption(a:Double, b: Double, c:Int):Int {

           var a1 = a.times(weight.toDouble())
           var a2 = b.times(height)
           var a3 = a1 + a2
           var a4 = a3.toInt() + c


           //(a.times(weight.toDouble()).plus(b.times(height.toDouble()))).toInt().plus(c)


           return a4
       }
   */
*/