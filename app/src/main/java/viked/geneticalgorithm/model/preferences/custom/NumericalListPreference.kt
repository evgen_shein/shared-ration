package viked.geneticalgorithm.model.preferences.custom

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.preference.DialogPreference
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceManager
import viked.geneticalgorithm.R

/**
 * Created by 1 on 07.04.2016.
 */
class NumericalListPreference(context:Context) : DialogPreference(context), INumericalPreference{

    var intEntries : List<Int> = listOf()
        set(value) {
            field = value
            this.entries = value.map { i -> i.toString() }.toTypedArray()
        }

    var entries : Array<String> = arrayOf()

    override var value : Int = 0
        set(value) {
            if(field.equals(value).not()) {
                field = value
                if(this.persistInt(value)){
                    this.notifyChanged()
                }
                this.summary = value.toString()
            }
        }

    fun getIndex() : Int{
        return intEntries.indexOf(value)
    }


    fun setValueByIndex(index : Int) {
        value = intEntries[index]
    }




    override fun onSetInitialValue(restorePersistedValue: Boolean, defaultValue: Any?) {
        this.value = if (restorePersistedValue) this.getPersistedInt(this.value) else {
            if (defaultValue != null) {
                defaultValue as Int
            } else 0
        }
    }

    init {
        this.dialogLayoutResource = R.layout.numerical_preference_layout
        this.positiveButtonText = context.getText(android.R.string.ok)
        this.negativeButtonText = context.getText(android.R.string.cancel)
    }


    override fun onAttachedToHierarchy(preferenceManager: PreferenceManager?) {
        super.onAttachedToHierarchy(preferenceManager)
        preferenceManager?.sharedPreferences?.registerOnSharedPreferenceChangeListener { sharedPreferences, s -> if(s.equals(key)){
            this.value = this.getPersistedInt(this.value)
        } }
    }

    override fun onSaveInstanceState(): Parcelable {
        val superState = super.onSaveInstanceState()
        if (this.isPersistent) {
            return superState
        } else {
            val myState = SavedState(superState)
            myState.value = this.value
            return myState
        }
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        if(state!=null && state is SavedState){
            val tempState : SavedState = state
            this.value = tempState.value

        }else{
            super.onRestoreInstanceState(state)
        }
    }



    private class SavedState : BaseSavedState {
        var value = 0

        constructor(source: Parcel) : super(source) {
            this.value = source.readInt()
        }

        override fun writeToParcel(dest: Parcel, flags: Int) {
            super.writeToParcel(dest, flags)
            dest.writeInt(this.value)
        }

        constructor(superState: Parcelable) : super(superState) {
        }

        companion object {

            val CREATOR = object : Parcelable.Creator<SavedState>{
                override fun createFromParcel(source: Parcel): SavedState? {
                    return SavedState(source)
                }

                override fun newArray(size: Int): Array<SavedState?> {
                    return arrayOfNulls(size)
                }
            }

        }
    }





}