package viked.geneticalgorithm.model.preferences.custom

/**
 * Created by 1 on 12.04.2016.
 */
interface INumericalPreference {
    var value : Int
}