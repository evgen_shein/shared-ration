package viked.geneticalgorithm.model.preferences

import android.content.Context
import android.preference.PreferenceManager
import viked.geneticalgorithm.MyApplication

/**
 * Created by 1 on 11.04.2016.
 */
class PreferencesModelImpl(var context: Context) : IPreferencesModel {

    init {
        MyApplication.graph.inject(this)
    }

    val preferences: MutableMap<String, Int> = mutableMapOf()

    private val INITIAL_KEY = "init"

    private var proteinsEnergyDistribution = 0.11
    private var fatsEnergyDistribution = 0.25
    private var carbohydratesDistribution = 0.64


    private var breakfastEnergyDistribution = 0.20
    private var lunchEnergyDistribution = 0.15
    private var dinnerEnergyDistribution = 0.45
    private var eveningMealEnergyDistribution = 0.20

    override fun loadPreferences() {
        preferences.putAll(getDefaultPreferences())
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        if (prefs.getBoolean(INITIAL_KEY, false)) {
            preferences.keys.forEach { key -> preferences[key] = prefs.getInt(key, preferences[key]!!) }
        } else {
            val editor = prefs.edit()
            editor.putBoolean(INITIAL_KEY, true)
            preferences.forEach { e -> editor.putInt(e.key, e.value) }
            editor.commit()
        }
    }

    override fun updatePreference(key: String, value: Int) {
        preferences[key] = value
    }

    override fun getDefaultPreferences(): Map<String, Int> {
        return mapOf(
                Pair(POPULATION_SIZE_KEY, populationSizeDefaultValue),
                Pair(CALORIES_TOLERANCE_KEY, caloriesToleranceDefaultValue),
                Pair(HARD_MUTATION_RATE_KEY, hardMutationRateDefaultValue),
                Pair(NEW_INDIVIDUALS_KEY, newIndividualsInPopulationDefaultValue),
                Pair(ENERGY_CONSUMPTION_KEY, energyConsumptionDefaultValue),
                Pair(WEIGHT_UNIT_KEY, weightUnitDefaultValue),
                Pair(FIRST_MEAL_CHANCE_KEY, firstMealChanceDefaultValue),
                Pair(SECOND_MEAL_CHANCE_KEY, secondMealChanceDefaultValue),
                Pair(VEGETABLES_CHANCE_KEY, vegetablesChanceDefaultValue),
                Pair(FRUIT_CHANCE_KEY, fruitChanceDefaultValue),
                Pair(BEVARAGES_CHANCE_KEY, beveragesChanceDefaultValue))
    }


    override fun getActualDistribution(): List<Double> {
        val energyConsumption = preferences[ENERGY_CONSUMPTION_KEY]!!.toDouble()
        return listOf(breakfastEnergyDistribution * energyConsumption,
                lunchEnergyDistribution * energyConsumption,
                dinnerEnergyDistribution * energyConsumption,
                eveningMealEnergyDistribution * energyConsumption)
    }

    override fun getMealChanceList(): List<Double> {
        return listOf(firstMealChance(), secondMealChance(),
                vegetablesChance(), fruitChance(), beveragesChance())
    }

    override fun beveragesChance(): Double = preferences[BEVARAGES_CHANCE_KEY]!!.toDouble() / 100.0

    override fun breakfastEnergyDistribution(): Double = breakfastEnergyDistribution

    override fun caloriesTolerance(): Double = preferences[CALORIES_TOLERANCE_KEY]!!.toDouble() / 100.0

    override fun carbohydratesDistribution(): Double = carbohydratesDistribution

    override fun dinnerEnergyDistribution(): Double = dinnerEnergyDistribution

    override fun energyConsumption(): Int = preferences[ENERGY_CONSUMPTION_KEY]!!

    override fun eveningMealEnergyDistribution(): Double = eveningMealEnergyDistribution

    override fun fatsEnergyDistribution(): Double = fatsEnergyDistribution

    override fun firstMealChance(): Double = preferences[FIRST_MEAL_CHANCE_KEY]!!.toDouble() / 100.0

    override fun fruitChance(): Double = preferences[FRUIT_CHANCE_KEY]!!.toDouble() / 100.0

    override fun hardMutationChance(): Double = preferences[HARD_MUTATION_RATE_KEY]!!.toDouble() / 100.0

    override fun lunchEnergyDistribution(): Double = lunchEnergyDistribution

    override fun newIndividualsInPopulation(): Double = preferences[NEW_INDIVIDUALS_KEY]!!.toDouble() / 100.0

    override fun populationSize(): Int = preferences[POPULATION_SIZE_KEY]!!

    override fun proteinsEnergyDistribution(): Double = proteinsEnergyDistribution

    override fun secondMealChance(): Double = preferences[SECOND_MEAL_CHANCE_KEY]!!.toDouble() / 100.0

    override fun vegetablesChance(): Double = preferences[VEGETABLES_CHANCE_KEY]!!.toDouble() / 100.0

    override fun weightUnit(): Int = preferences[WEIGHT_UNIT_KEY]!!
}