package viked.geneticalgorithm.model.preferences


/**
 * Created by 1 on 11.04.2016.
 */
interface IPreferencesModel {

    /*
    * Base io
    * */
    fun loadPreferences()

    fun getDefaultPreferences() : Map<String, Int>

    fun updatePreference(key : String, value:Int)

    /*
    * Human preferences
    * */
    fun energyConsumption() : Int
    fun weightUnit() : Int

    fun firstMealChance() : Double
    fun secondMealChance() : Double
    fun vegetablesChance() : Double
    fun fruitChance() : Double
    fun beveragesChance() : Double

    fun proteinsEnergyDistribution()  : Double
    fun fatsEnergyDistribution()  : Double
    fun carbohydratesDistribution()  : Double

    fun breakfastEnergyDistribution()   : Double
    fun lunchEnergyDistribution()  : Double
    fun dinnerEnergyDistribution()  : Double
    fun eveningMealEnergyDistribution()  : Double

    fun getMealChanceList() : List<Double>

    fun getActualDistribution(): List<Double>

    /*
    * Algorithm preferences
    * */
    fun populationSize() : Int

    fun caloriesTolerance () : Double

    fun hardMutationChance (): Double

    fun newIndividualsInPopulation() : Double

}