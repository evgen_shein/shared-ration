package viked.geneticalgorithm.model.preferences

/**
 * Created by 1 on 07.04.2016.
 */



const val POPULATION_SIZE_KEY = "population_size"
const val CALORIES_TOLERANCE_KEY = "error"
const val HARD_MUTATION_RATE_KEY = "mutation_rate"
const val NEW_INDIVIDUALS_KEY = "new_individuals"

const val ENERGY_CONSUMPTION_KEY = "energy_consumption"

const val WEIGHT_UNIT_KEY = "weight_unit"

const val FIRST_MEAL_CHANCE_KEY = "first_meal_chance"
const val SECOND_MEAL_CHANCE_KEY = "second_meal_chance"
const val VEGETABLES_CHANCE_KEY = "vegetables_chance"
const val FRUIT_CHANCE_KEY = "fruit_chance"
const val BEVARAGES_CHANCE_KEY = "beverages_chance"



/*
* Human preferences default values
* */
const val energyConsumptionDefaultValue = 2500
const val firstMealChanceDefaultValue = 35
const val secondMealChanceDefaultValue = 100
const val vegetablesChanceDefaultValue = 30
const val fruitChanceDefaultValue = 25
const val beveragesChanceDefaultValue = 90

const val weightUnitDefaultValue = 50
val weightUnitList = listOf(1,5,10,50,100)

/*
* Algorithm preferences default values
* */
const val populationSizeDefaultValue = 1000
val populationSizeList = listOf(10,50,100,500,1000,5000,10000)

const val caloriesToleranceDefaultValue = 1
const val hardMutationRateDefaultValue = 10
const val newIndividualsInPopulationDefaultValue = 5

