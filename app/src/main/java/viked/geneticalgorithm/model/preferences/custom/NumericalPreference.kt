package viked.geneticalgorithm.model.preferences.custom

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.preference.DialogPreference
import android.support.v7.preference.PreferenceManager
import viked.geneticalgorithm.R

/**
 * Created by 1 on 25.03.2016.
 */
class NumericalPreference(context: Context) : DialogPreference(context), INumericalPreference {

    var baseSummary = ""
        set(value) {
            field = value
            this.summary = this.value.toString()
        }

    var min = 0

    var max = 100

    override var value : Int = 0
        set(value) {
            if(field.equals(value).not()) {
                field = value
                if(this.persistInt(value)){
                    this.summary = value.toString()
                    this.notifyChanged()
                }
            }
        }

    override fun onSetInitialValue(restorePersistedValue: Boolean, defaultValue: Any?) {
        this.value = if (restorePersistedValue) this.getPersistedInt(this.value) else {
            if (defaultValue != null) {
                defaultValue as Int
            } else 0
        }
    }

    override fun onAttachedToHierarchy(preferenceManager: PreferenceManager?) {
        super.onAttachedToHierarchy(preferenceManager)
        preferenceManager?.sharedPreferences?.registerOnSharedPreferenceChangeListener { sharedPreferences, s -> if(s.equals(key)){
            this.value = this.getPersistedInt(this.value)
        } }
    }

    override fun setSummary(summary: CharSequence?) {
        super.setSummary("$baseSummary $summary%")
    }

    init {
        this.dialogLayoutResource = R.layout.numerical_preference_layout
        this.positiveButtonText = context.getText(android.R.string.ok)
        this.negativeButtonText = context.getText(android.R.string.cancel)
    }

    override fun onSaveInstanceState(): Parcelable {
        val superState = super.onSaveInstanceState()
        if (this.isPersistent) {
            return superState
        } else {
            val myState = SavedState(superState)
            myState.value = this.value
            return myState
        }
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        if(state!=null && state is SavedState){
            val tempState : SavedState = state
            this.value = tempState.value

        }else{
            super.onRestoreInstanceState(state)
        }
    }



    private class SavedState : BaseSavedState {
        var value = 0

        constructor(source: Parcel) : super(source) {
            this.value = source.readInt()
        }

        override fun writeToParcel(dest: Parcel, flags: Int) {
            super.writeToParcel(dest, flags)
            dest.writeInt(this.value)
        }

        constructor(superState: Parcelable) : super(superState) {
        }

        companion object {

            val CREATOR = object : Parcelable.Creator<SavedState>{
                override fun createFromParcel(source: Parcel): SavedState? {
                    return SavedState(source)
                }

                override fun newArray(size: Int): Array<SavedState?> {
                    return arrayOfNulls(size)
                }
            }

        }
    }
}