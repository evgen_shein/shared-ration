
package viked.geneticalgorithm.model.data.objects;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class DBModel implements RealmModel {

    @PrimaryKey
    private Integer version;
    private Long lastUpdate;
    private RealmList<Dish> foodList;
    private RealmList<WeightLimit> weightLimit;

    public Integer getVersion() {
        return version;
    }

    public Long getLastUpdate() {
        return lastUpdate;
    }

    public List<Dish> getFoodList() {
        return foodList;
    }

    public List<WeightLimit> getWeightLimit() {
        return weightLimit;
    }

    public static DBModel getDBModel(Realm realm) {
        return realm.where(DBModel.class).findFirst();
    }

}
