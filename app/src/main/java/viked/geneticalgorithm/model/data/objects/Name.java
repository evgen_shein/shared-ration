
package viked.geneticalgorithm.model.data.objects;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

@RealmClass
public class Name implements RealmModel {

    private String index;
    private String name;

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
