package viked.geneticalgorithm.model.data

import android.content.Context
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.*
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmObject
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import viked.geneticalgorithm.BuildConfig
import viked.geneticalgorithm.MyApplication
import viked.geneticalgorithm.model.VersionException
import viked.geneticalgorithm.model.data.containers.DishContainer
import viked.geneticalgorithm.model.data.objects.DBModel
import viked.geneticalgorithm.model.data.objects.Dish
import viked.geneticalgorithm.model.data.objects.RealmInt

/**
 * Created by 1 on 22.03.2016.
 */
class DataBaseModel(val context: Context) {

    init {
        MyApplication.graph.inject(this)
    }

    fun load(): Observable<Boolean> {
        val observable: Observable<String> =
        // if (BuildConfig.DEBUG) {
                if (false) {
                    Observable.just(context.resources.getIdentifier("db_model", "raw", context.packageName))
                            .map { context.resources.openRawResource(it) }
                            .map { it.bufferedReader() }
                            .map { it.readLines() }
                            .map { it.reduce { acc, s -> acc + s } }
                } else {
                    Observable.create<String> {
                        val reference = FirebaseDatabase.getInstance().reference
                        reference.addValueEventListener(object : ValueEventListener {

                            override fun onCancelled(p0: DatabaseError) {
                                it.onError(p0?.toException() ?: NullPointerException())
                            }

                            override fun onDataChange(p0: DataSnapshot) {
                                if (p0 == null) {
                                    it.onError(VersionException())
                                } else {
                                    it.onNext(Gson().toJson(p0.value))
                                }
                            }
                        })
                    }.take(1)
                }
        return observable.map { it.getDBModel() }
                .doOnNext { it.update() }
                .map { true }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    private fun DBModel.update() {
        if (this.version.equals(BuildConfig.DB_MODEL_VERSION)) {
            val realm = Realm.getDefaultInstance()
            val lastDbModel: DBModel? = DBModel.getDBModel(realm)
            when {
                lastDbModel?.lastUpdate?.equals(this.lastUpdate)?.not() ?: true -> {
                    realm.executeTransaction {
                        realm.copyToRealmOrUpdate(this)
                        if (this.foodList.size.equals(lastDbModel?.foodList?.size).not()) {
                            this.foodList.generateContainers(realm)
                        }
                    }
                }
                DishContainer.getAll(realm).isEmpty() -> realm.executeTransaction {
                    this.foodList.generateContainers(realm)
                }
            }
        } else {
            throw VersionException()
        }
    }

    private fun List<Dish>.generateContainers(realm: Realm) {
        this.map { DishContainer(it.id, it) }
                .forEach { realm.copyToRealmOrUpdate(it) }
    }

    private fun String.getDBModel(): DBModel {
        val gson = GsonBuilder()
                .setExclusionStrategies(exclusionStrategy)
                .registerTypeAdapter(integerListToken, integerListTypeAdapter).create()
        return gson.fromJson<DBModel>(this, object : TypeToken<DBModel>() {}.type!!)
    }


    private val exclusionStrategy = object : ExclusionStrategy {
        override fun shouldSkipClass(clazz: Class<*>?): Boolean {
            return false
        }

        override fun shouldSkipField(f: FieldAttributes): Boolean {
            return f.declaringClass.equals(RealmObject::class.java)
        }
    }

    private val integerListToken = object : TypeToken<RealmList<RealmInt>>() {}.type!!

    private val integerListTypeAdapter = object : TypeAdapter<RealmList<RealmInt>>() {
        override fun write(out: JsonWriter, value: RealmList<RealmInt>) {

        }

        override fun read(`in`: JsonReader): RealmList<RealmInt> {
            val list = RealmList<RealmInt>()
            `in`.beginArray()
            while (`in`.hasNext()) {
                list.add(RealmInt(`in`.nextInt()))
            }
            `in`.endArray()
            return list
        }
    }

}