
package viked.geneticalgorithm.model.data.objects;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

@RealmClass
public class Element implements RealmModel {

    private String name;
    private Double weight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

}
