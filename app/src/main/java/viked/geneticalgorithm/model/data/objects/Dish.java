
package viked.geneticalgorithm.model.data.objects;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Dish implements RealmModel {

    @PrimaryKey
    private Long id;

    private Long type;
    private RealmList<Name> name;
    private RealmList<RealmInt> filter;
    private RealmList<Element> elements;
    private Double proteins;
    private Double fats;
    private Double carbohydrates;

    public Long getId() {
        return id;
    }

    public Long getType() {
        return type;
    }

    public List<Name> getName() {
        return name;
    }

    public List<RealmInt> getFilter() {
        return filter;
    }

    public List<Element> getElements() {
        return elements;
    }

    public Double getProteins() {
        return proteins;
    }

    public Double getFats() {
        return fats;
    }

    public Double getCarbohydrates() {
        return carbohydrates;
    }

    public static Dish getDish(Realm realm, Long id) {
        return realm.where(Dish.class).equalTo("id", id).findFirst();
    }

    public static List<Dish> getAll(Realm realm) {
        return realm.where(Dish.class).findAll();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dish dish = (Dish) o;

        if (id != null ? !id.equals(dish.id) : dish.id != null) return false;
        if (type != null ? !type.equals(dish.type) : dish.type != null) return false;
        if (name != null ? !name.equals(dish.name) : dish.name != null) return false;
        if (filter != null ? !filter.equals(dish.filter) : dish.filter != null) return false;
        if (elements != null ? !elements.equals(dish.elements) : dish.elements != null)
            return false;
        if (proteins != null ? !proteins.equals(dish.proteins) : dish.proteins != null)
            return false;
        if (fats != null ? !fats.equals(dish.fats) : dish.fats != null) return false;
        return carbohydrates != null ? carbohydrates.equals(dish.carbohydrates) : dish.carbohydrates == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (filter != null ? filter.hashCode() : 0);
        result = 31 * result + (elements != null ? elements.hashCode() : 0);
        result = 31 * result + (proteins != null ? proteins.hashCode() : 0);
        result = 31 * result + (fats != null ? fats.hashCode() : 0);
        result = 31 * result + (carbohydrates != null ? carbohydrates.hashCode() : 0);
        return result;
    }
}
