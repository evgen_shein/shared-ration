
package viked.geneticalgorithm.model.data.containers;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;
import viked.geneticalgorithm.model.data.objects.Dish;

@RealmClass
public class DishContainer implements RealmModel {

    @PrimaryKey
    private Long id;

    private Dish dish;

    private Boolean check = true;

    public DishContainer() {
    }

    public DishContainer(Long id, Dish dish) {
        this.id = id;
        this.dish = dish;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public Boolean getCheck() {
        return check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    @NonNull
    public static List<DishContainer> getAll(Realm realm) {
        return realm.where(DishContainer.class).findAll();
    }

    @NonNull
    public static List<DishContainer> getAllSelected(Realm realm) {
        return realm.where(DishContainer.class).equalTo("check", true).findAll();
    }

    @NonNull
    public static List<List<DishContainer>> getTypedList(Realm realm) {
        List<List<DishContainer>> out = new ArrayList<>(5);
        for (int type = 1; type <= 5; type++) {
            out.add(realm.where(DishContainer.class).equalTo("dish.type", type).equalTo("check", true).findAll());
        }
        return out;
    }


}
