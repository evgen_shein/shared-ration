package viked.geneticalgorithm.model.data.objects;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

/**
 * Created by 1 on 09.07.2016.
 */
@RealmClass
public class RealmInt implements RealmModel {

    public RealmInt() {
    }

    public RealmInt(int value) {
        this.value = value;
    }

    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
