
package viked.geneticalgorithm.model.data.objects;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class WeightLimit implements RealmModel {
    @PrimaryKey
    private Long type;
    private Long min;
    private Long max;

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getMin() {
        return min;
    }

    public void setMin(Long min) {
        this.min = min;
    }

    public Long getMax() {
        return max;
    }

    public void setMax(Long max) {
        this.max = max;
    }

    @NonNull
    public static List<WeightLimit> getTyped(Realm realm) {
        List<WeightLimit> out = new ArrayList<>(5);
        for (int type = 1; type <= 5; type++) {
            out.add(realm.where(WeightLimit.class).equalTo("type", type).findFirst());
        }
        return out;
    }

}
