package viked.geneticalgorithm.model.data.objects;

import android.support.annotation.NonNull;

import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by 1 on 09.07.2016.
 */
@RealmClass
public class User implements RealmModel {

    @PrimaryKey
    private String id;

    private String first_name;

    private String last_name;

    private Integer sex;

    //DD.MM.YYYY
    private String bdate;

    private String photo_200;

    @NonNull
    public String getFirst_name() {
        return first_name;
    }

    @NonNull
    public String getLast_name() {
        return last_name;
    }

    @NonNull
    public Integer getSex() {
        return sex;
    }

    @NonNull
    public String getBdate() {
        return bdate;
    }

    @NonNull
    public String getPhoto_200() {
        return photo_200;
    }

    @NonNull
    public String getId() {
        return id != null ? id : "";
    }

    public static User getUser(@NonNull Realm realm) {
        return realm.where(User.class).findFirst();
    }

    @NonNull
    public static User createUser(@NonNull Realm realm, @NonNull final JSONObject userObject) {
        realm.beginTransaction();
        User user = realm.createOrUpdateObjectFromJson(User.class, userObject);
        realm.commitTransaction();
        return user;
    }

    public static void clearUser(@NonNull Realm realm) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(User.class);
            }
        });
    }
}
