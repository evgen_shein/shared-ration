package viked.geneticalgorithm.model.genetic.algorithm.orders

import viked.algorithmlibrary.data.Order

/**
 * Created by Viked on 9/10/2016.
 */
class MealOrder(val type: Int) : Order