package viked.geneticalgorithm.model.genetic.algorithm.orders

import viked.algorithmlibrary.data.Order

/**
 * Created by Viked on 9/10/2016.
 */
class PortionOrder(val type: Int, val filter: Int) : Order