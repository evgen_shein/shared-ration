package viked.geneticalgorithm.model.genetic.algorithm.objects;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import viked.algorithmlibrary.data.Chromosome;
import viked.algorithmlibrary.data.Gene;

/**
 * Created by Viked on 7/16/2016.
 */

public class MealItem implements IAlgorithmItem<Meal>, Chromosome {

    private int type;

    private List<Gene> portionList;

    @Override
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @NotNull
    @Override
    public List<Gene> getGenotype() {
        if (portionList == null) {
            portionList = new ArrayList<>();
        }
        return portionList;
    }

    @Override
    public void setRealmObject(Meal meal) {
        portionList = new ArrayList<>();
        for (Portion portion : meal.getPortionRealmList()) {
            PortionItem item = new PortionItem();
            item.setRealmObject(portion);
            portionList.add(item);
        }
    }

    @Override
    public Meal getRealmObject(@NotNull Realm realm) {
        Meal meal = new Meal();
        for (Gene gene : portionList) {
            meal.addPortion(((PortionItem) gene).getRealmObject(realm));
        }
        return meal;
    }

    @Override
    public Chromosome clone() {
        MealItem mealItem = new MealItem();
        mealItem.type = type;
        for (Gene item : portionList) {
            mealItem.getGenotype().add(item.clone());
        }
        return mealItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MealItem mealItem = (MealItem) o;

        if (type != mealItem.type || portionList.size() != mealItem.portionList.size())
            return false;

        for (int i = portionList.size() - 1; i > -1; i--) {
            if (!portionList.get(i).equals(mealItem.portionList.get(i))) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = type;
        result = 31 * result + portionList.hashCode();
        return result;
    }

    @NotNull
    @Override
    public double[] getValues() {
        double[] values = new double[]{0.0, 0.0, 0.0, 0.0};
        for (Gene gene : portionList) {
            PortionItem portion = (PortionItem) gene;
            double[] portionValues = portion.getValues();
            for (int i = 0; i < portionValues.length; i++) {
                values[i] += portionValues[i];
            }
        }
        return values;
    }
}
