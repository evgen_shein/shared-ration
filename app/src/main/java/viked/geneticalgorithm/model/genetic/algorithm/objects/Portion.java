package viked.geneticalgorithm.model.genetic.algorithm.objects;

import java.util.List;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;
import viked.geneticalgorithm.model.ConstantsKt;
import viked.geneticalgorithm.model.data.objects.Dish;

/**
 * Created by Viked on 7/16/2016.
 */
@RealmClass
public class Portion implements RealmModel {

    private Dish dish;

    private int weight;

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }


    public double[] getValues() {
        return getValues(weight);
    }

    public double[] getValues(double weight) {
        double[] values = new double[]{
                getIncrementedValue(dish.getProteins(), weight),
                getIncrementedValue(dish.getFats(), weight),
                getIncrementedValue(dish.getCarbohydrates(), weight),
                0.0
        };
        List<Double> unitIndexes = ConstantsKt.getUnitIndexes();
        for (int i = 0; i < unitIndexes.size(); i++) {
            values[values.length - 1] += values[i] * unitIndexes.get(i);
        }
        return values;
    }

    private double getIncrementedValue(double value, double weight) {
        return value / 100.0 * weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Portion portion = (Portion) o;

        if (weight != portion.weight) return false;
        return dish != null ? dish.equals(portion.dish) : portion.dish == null;

    }

    @Override
    public int hashCode() {
        int result = dish != null ? dish.hashCode() : 0;
        result = 31 * result + weight;
        return result;
    }
}
