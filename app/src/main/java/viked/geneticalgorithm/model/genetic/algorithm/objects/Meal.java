package viked.geneticalgorithm.model.genetic.algorithm.objects;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

/**
 * Created by Viked on 7/16/2016.
 */
@RealmClass
public class Meal implements RealmModel {

    private RealmList<Portion> portionRealmList;

    public void addPortion(Portion portion) {
        if (portionRealmList == null) {
            portionRealmList = new RealmList<>();
        }
        portionRealmList.add(portion);
    }

    public RealmList<Portion> getPortionRealmList() {
        return portionRealmList;
    }

    public void setPortionRealmList(RealmList<Portion> portionRealmList) {
        this.portionRealmList = portionRealmList;
    }

    public double[] getValues() {
        double[] values = new double[]{0.0, 0.0, 0.0, 0.0};
        for(Portion portion: portionRealmList){
            double[] portionValues = portion.getValues();
            for (int i = 0; i< portionValues.length; i++){
                values[i]+=portionValues[i];
            }
        }
        return values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Meal meal = (Meal) o;

        return portionRealmList != null ? portionRealmList.equals(meal.portionRealmList) : meal.portionRealmList == null;

    }

    @Override
    public int hashCode() {
        return portionRealmList != null ? portionRealmList.hashCode() : 0;
    }
}
