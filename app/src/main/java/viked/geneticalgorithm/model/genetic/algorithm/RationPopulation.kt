package viked.geneticalgorithm.model.genetic.algorithm

import viked.algorithmlibrary.data.*
import viked.geneticalgorithm.model.genetic.algorithm.orders.MealOrder
import viked.geneticalgorithm.state.AlgorithmState

/**
 * Created by 1 on 22.03.2016.
 */
class RationPopulation(override val seed: Seed) : Population {

    override var iteration: Int = 0

    override val individuals: MutableList<Individual> = mutableListOf()

    override val newIndividuals: MutableList<Individual> = mutableListOf()

    override var complete: Boolean = false

    override fun getState(): IAlgorithmState {
        val state = AlgorithmState(
                newIndividuals.size.toDouble() / seed.populationSize.toDouble(),
                iteration,
                complete)
        return state
    }

    override fun getMinFitness(): Double = individuals.minBy { it.fitness }?.fitness ?: Double.MAX_VALUE

    override fun getMaxFitness(): Double = individuals.maxBy { it.fitness }?.fitness ?: Double.MIN_VALUE

    override fun getMedianFitness(): Double = individuals.sumByDouble { it.fitness } / seed.populationSize

    override fun getChromosomeOrder(type: Int): Order = MealOrder(type)
}