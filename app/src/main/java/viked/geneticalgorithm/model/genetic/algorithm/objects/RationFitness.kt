package viked.geneticalgorithm.model.genetic.algorithm.objects

import viked.algorithmlibrary.data.Individual
import viked.algorithmlibrary.fitness.IFitness

/**
 * Created by Viked on 9/4/2016.
 */
class RationFitness(val energyConsumption: List<Double>,
                    val energyDistribution: List<Double>) : IFitness {

    private val count = 3

    private val unitIndexes = listOf(4.1, 9.3, 4.1)

    override fun calculate(individual: Individual): Double {
        val totalEnergy = Array(count, { 0.0 })
        var fitness = 0.0
        individual.genotype.forEachIndexed { i, meal ->
            val mealEnergy = (meal as MealItem).getValues()
            fitness += Math.abs(energyDistribution[i] - mealEnergy.last())
            (0..(count - 1)).forEach { totalEnergy[it] += mealEnergy[it] * unitIndexes[it] }
        }
        fitness += energyConsumption
                .zip(totalEnergy) { energyConsumption, totalEnergy -> Math.abs(energyConsumption - totalEnergy) }
                .sum()
        return fitness
    }
}