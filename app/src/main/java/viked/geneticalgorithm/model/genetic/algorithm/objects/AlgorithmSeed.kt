package viked.geneticalgorithm.model.genetic.algorithm.objects

import viked.algorithmlibrary.data.Gene
import viked.algorithmlibrary.data.Seed
import viked.algorithmlibrary.fitness.IFitness
import viked.algorithmlibrary.initial.IInitialisation

/**
 * Created by Viked on 9/4/2016.
 */
class AlgorithmSeed(override val populationSize: Int,
                    override val genePool: List<Gene>,
                    val energyDistribution: List<Double>,
                    val energyConsumption: List<Double>,
                    val totalEnergyConsumption: Double,
                    val caloriesTolerance: Double,
                    val foodWeighRange: List<IntRange>,
                    val mealChanceList: List<Double>,
                    val weightUnit: Double) : Seed {

    override val error: Double = totalEnergyConsumption * caloriesTolerance

    override val chromosomeCount: Int = 4

    override val geneCount: Int = 5

    override val savePopulationSize: Int = 10

    override val filter: List<Int> = listOf()

    override val fitness: IFitness = RationFitness(energyConsumption, energyDistribution)

    override val initialisation: IInitialisation = AlgorithmInitialisation(this)

}