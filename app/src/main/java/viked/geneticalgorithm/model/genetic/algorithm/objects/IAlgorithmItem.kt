package viked.geneticalgorithm.model.genetic.algorithm.objects

import io.realm.Realm

/**
 * Created by Viked on 8/31/2016.
 */
interface IAlgorithmItem<RealmObject> {
    fun setRealmObject(realmObject: RealmObject)
    fun getRealmObject(realm: Realm): RealmObject
    fun getValues(): DoubleArray
}