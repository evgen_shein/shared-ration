package viked.geneticalgorithm.model.genetic.algorithm.objects;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import viked.algorithmlibrary.data.Gene;
import viked.geneticalgorithm.model.data.objects.Dish;
import viked.geneticalgorithm.model.data.objects.Element;
import viked.geneticalgorithm.model.data.objects.RealmInt;
import viked.geneticalgorithm.model.genetic.algorithm.ConstantsKt;

/**
 * Created by Viked on 9/1/2016.
 */
public class PortionItem implements IAlgorithmItem<Portion>, Gene {

    private Long dishId;

    private Long type;

    private String name;

    private List<Integer> filter;

    private List<Element> elements;

    private Double proteins;

    private Double fats;

    private Double carbohydrates;

    private int weight;

    @Override
    public int getType() {
        return type != null ? type.intValue() : ConstantsKt.EMPTY_TYPE;
    }

    @NotNull
    @Override
    public List<Integer> getFilter() {
        return filter != null ? filter : Collections.<Integer>emptyList();
    }

    @Override
    public void setRealmObject(Portion portion) {
        dishId = portion.getDish().getId();
        type = portion.getDish().getType();
        name = portion.getDish().getName().get(0).getName();
        //elements = portion.getDish().getElements();
        proteins = portion.getDish().getProteins();
        fats = portion.getDish().getFats();
        carbohydrates = portion.getDish().getCarbohydrates();
        weight = portion.getWeight();

        filter = new ArrayList<>();
        for (RealmInt i : portion.getDish().getFilter()) {
            filter.add(i.getValue());
        }
    }

    @Override
    public Portion getRealmObject(@NotNull Realm realm) {
        Portion portion = new Portion();
        portion.setWeight(weight);
        portion.setDish(Dish.getDish(realm, dishId));
        return portion;
    }

    @NotNull
    @Override
    public double[] getValues() {
        double[] values = new double[]{
                getIncrementedValue(proteins, weight),
                getIncrementedValue(fats, weight),
                getIncrementedValue(carbohydrates, weight),
                0.0
        };
        List<Double> unitIndexes = viked.geneticalgorithm.model.ConstantsKt.getUnitIndexes();
        for (int i = 0; i < unitIndexes.size(); i++) {
            values[values.length - 1] += values[i] * unitIndexes.get(i);
        }
        return values;
    }

    @Override
    public Gene clone() {
        PortionItem clone = new PortionItem();
        clone.dishId = dishId;
        clone.type = type;
        clone.name = name;
        clone.filter = filter;
        clone.elements = elements;
        clone.proteins = proteins;
        clone.fats = fats;
        clone.carbohydrates = carbohydrates;
        clone.weight = weight;
        return clone;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @NotNull
    public String getName() {
        /*
        if (name != null && !name.isEmpty()) {
            return name.get(0).getName();
        } else {
            return "";
        }*/
        return name;
    }

    @NotNull
    public Double getProteins() {
        return proteins != null ? proteins : 0.0;
    }

    @NotNull
    public Double getFats() {
        return fats != null ? fats : 0.0;
    }

    @NotNull
    public Double getCarbohydrates() {
        return carbohydrates != null ? carbohydrates : 0.0;
    }

    private double getIncrementedValue(double value, double weight) {
        return value / 100.0 * weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PortionItem that = (PortionItem) o;

        return weight == that.weight && dishId.equals(that.dishId);
    }

    @Override
    public int hashCode() {
        int result = dishId.hashCode();
        result = 31 * result + weight;
        return result;
    }
}
