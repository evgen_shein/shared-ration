package viked.geneticalgorithm.model.genetic.algorithm

import io.realm.Realm
import rx.subjects.PublishSubject
import viked.algorithmlibrary.BaseAlgorithmModel
import viked.algorithmlibrary.data.Population
import viked.algorithmlibrary.data.Seed
import viked.algorithmlibrary.doForEachIndexed
import viked.geneticalgorithm.MyApplication
import viked.geneticalgorithm.model.data.containers.DishContainer
import viked.geneticalgorithm.model.genetic.algorithm.objects.AlgorithmSeed
import viked.geneticalgorithm.model.genetic.algorithm.objects.Portion
import viked.geneticalgorithm.model.genetic.algorithm.objects.PortionItem
import viked.geneticalgorithm.model.genetic.algorithm.objects.RationItem
import viked.geneticalgorithm.model.getTypedWeightLimitList
import viked.geneticalgorithm.model.preferences.IPreferencesModel
import viked.geneticalgorithm.state.IApplicationState
import javax.inject.Inject

/**
 * Created by 1 on 22.03.2016.
 */
class GeneticAlgorithmModel : BaseAlgorithmModel() {

    init {
        MyApplication.graph.inject(this)
    }

    @Inject
    lateinit var preferences: IPreferencesModel

    @Inject
    lateinit var updateSubject: PublishSubject<IApplicationState>

    override fun getSeed(): Seed {
        val realm = Realm.getDefaultInstance()
        val weightUnit = preferences.weightUnit()
        val energyConsumption = preferences.energyConsumption().toDouble()
        val seed = AlgorithmSeed(preferences.populationSize(),
                DishContainer.getAllSelected(realm).map { it.dish }
                        .map {
                            val portion = Portion()
                            portion.dish = it
                            portion
                        }
                        .map {
                            val portion = PortionItem()
                            portion.setRealmObject(it)
                            portion
                        },
                preferences.getActualDistribution(),
                listOf(preferences.proteinsEnergyDistribution() * energyConsumption,
                        preferences.fatsEnergyDistribution() * energyConsumption,
                        preferences.carbohydratesDistribution() * energyConsumption),
                energyConsumption,
                preferences.caloriesTolerance(),
                getTypedWeightLimitList(realm)
                        .map { IntRange(it.start / weightUnit, it.endInclusive / weightUnit) },
                preferences.getMealChanceList(),
                weightUnit.toDouble())
        return seed
    }

    override fun getPopulation(): Population = RationPopulation(getSeed())

    override fun isEndCondition(population: Population): Boolean =
            population.getMinFitness() < population.seed.error

    override fun save(population: Population) {
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            population.individuals.take(population.seed.savePopulationSize)
                    .map { it as RationItem }
                    .map { it.getRealmObject(realm) }
                    .doForEachIndexed { i, ration -> ration.id = (i + 1).toLong() }
                    .forEach { realm.copyToRealmOrUpdate(it) }
        }
    }
}