package viked.geneticalgorithm.model.genetic.algorithm.objects;

import android.support.annotation.NonNull;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Viked on 7/16/2016.
 */
@RealmClass
public class Ration implements RealmModel {

    @PrimaryKey
    private long id;

    private double fitness;

    private RealmList<Meal> mealRealmList;

    public void addMeal(Meal meal) {
        if (mealRealmList == null) {
            mealRealmList = new RealmList<>();
        }
        mealRealmList.add(meal);
    }

    public RealmList<Meal> getMealRealmList() {
        return mealRealmList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public void setMealRealmList(RealmList<Meal> mealRealmList) {
        this.mealRealmList = mealRealmList;
    }

    public double[] getValues() {
        double[] values = new double[]{0.0, 0.0, 0.0, 0.0};
        for(Meal portion: mealRealmList){
            double[] portionValues = portion.getValues();
            for (int i = 0; i< portionValues.length; i++){
                values[i]+=portionValues[i];
            }
        }
        return values;
    }

    @NonNull
    public static RealmResults<Ration> getAll(Realm realm) {
        return realm.where(Ration.class).findAll();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ration ration = (Ration) o;

        return mealRealmList != null ? mealRealmList.equals(ration.mealRealmList) : ration.mealRealmList == null;

    }

    @Override
    public int hashCode() {
        return mealRealmList != null ? mealRealmList.hashCode() : 0;
    }
}
