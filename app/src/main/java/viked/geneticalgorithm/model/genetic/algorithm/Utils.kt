package viked.geneticalgorithm.model.genetic.algorithm

import viked.geneticalgorithm.model.data.objects.Dish
import viked.geneticalgorithm.model.genetic.algorithm.objects.Meal
import viked.geneticalgorithm.model.genetic.algorithm.objects.Portion
import viked.geneticalgorithm.model.genetic.algorithm.objects.Ration

/**
 * Created by Viked on 7/16/2016.
 */


fun generateRation(mealCount: Int,
                   foodList: List<List<Dish>>,
                   mealChanceList: List<Double>,
                   foodWeighRange: List<IntRange>,
                   weightUnit: Int,
                   energyDistribution: List<Double>,
                   energyConsumption: List<Double>): Ration {
    val ration = Ration()
    (0..mealCount - 1).forEach { ration.addMeal(generateMeal(foodList, mealChanceList, foodWeighRange, weightUnit)) }
    ration.calculateFitness(energyDistribution, energyConsumption)
    return ration
}

fun generateMeal(foodList: List<List<Dish>>, mealChanceList: List<Double>, foodWeighRange: List<IntRange>, weightUnit: Int): Meal {
    val meal = Meal()
    (0..4).filter { Math.random() < mealChanceList[it] }
            .forEach {
                val portion = Portion()
                portion.dish = foodList[it][(Math.random() * foodList[it].size.toDouble()).toInt()]
                portion.weight = (foodWeighRange[it].start + Math.random() * (foodWeighRange[it].endInclusive -
                        (foodWeighRange[it].start))).toInt().times(weightUnit)
                meal.addPortion(portion)
            }
    return meal
}

fun Ration.calculateFitness(energyDistribution: List<Double>, energyConsumption: List<Double>) {
    val totalEnergy = arrayOf(0.0, 0.0, 0.0)
    fitness = 0.0
    mealRealmList.forEachIndexed { i, meal ->
        val mealEnergy = meal.getEnergy()
        fitness += Math.abs(energyDistribution[i] - mealEnergy.sum())
        mealEnergy.forEachIndexed { i, d -> totalEnergy[i] + d }
    }
    fitness += energyConsumption
            .zip(totalEnergy) { energyConsumption, totalEnergy -> Math.abs(energyConsumption - totalEnergy) }
            .sum()
}


fun Meal.getEnergy(): List<Double> {
    val energy = portionRealmList.map { it.getEnergy() }
    val out = arrayOf(0.0, 0.0, 0.0)
    energy.forEach { it.forEachIndexed { i, d -> out[i] += d } }
    return out.asList()
}

fun Portion.getEnergy(): List<Double> = listOf(
        (4.1 * (dish.proteins / 100.0 * weight.toDouble())),
        (9.3 * (dish.fats / 100.0 * weight.toDouble())),
        (4.1 * (dish.carbohydrates / 100.0 * weight.toDouble()))
)


fun Ration.copy(): Ration {
    val newRation = Ration()
    mealRealmList.map { it.copy() }.forEach { newRation.addMeal(it) }
    return newRation
}

fun Meal.copy(): Meal {
    val newMeal = Meal()
    portionRealmList.map { it.copy() }.forEach { newMeal.addPortion(it) }
    return newMeal

}

fun Portion.copy(): Portion {
    val newPortion = Portion()
    newPortion.dish = dish
    newPortion.weight = weight
    return newPortion
}