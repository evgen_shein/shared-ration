package viked.geneticalgorithm.model.genetic.algorithm.objects

import viked.algorithmlibrary.data.*
import viked.algorithmlibrary.initial.IInitialisation
import viked.geneticalgorithm.model.genetic.algorithm.orders.MealOrder
import viked.geneticalgorithm.model.genetic.algorithm.orders.PortionOrder

/**
 * Created by Viked on 9/4/2016.
 */
class AlgorithmInitialisation(override val seed: Seed) : IInitialisation {

    private val filteredFoodMap: Map<Int, List<List<Gene>>>

    private val filters: List<Int>

    init {
        val temp: MutableMap<Int, List<List<Gene>>> = mutableMapOf()
        filters = getFilters()

        val noFiltered = (1..5).map { type -> seed.genePool.filter { it.type == type && checkGene(it.filter, filters).not() } }

        filters.forEach { filter -> temp.put(filter, (1..5).map { type -> seed.genePool.filter { it.type == type && it.filter.contains(filter) } + noFiltered[type - 1] }) }

        filteredFoodMap = temp
    }


    override fun generateIndividual(): Individual {
        val ration = RationItem()
        ration.genotype.addAll((0..seed.chromosomeCount - 1)
                .map { generateChromosome(MealOrder(it)) })
        return ration
    }

    override fun generateChromosome(order: Order): Chromosome {
        val mealOrder = order as MealOrder
        val meal = MealItem()
        meal.setType(mealOrder.type)
        meal.genotype.addAll((0..seed.geneCount - 1)
                .filter { Math.random() < (seed as AlgorithmSeed).mealChanceList[it] }
                .map { generateGene(getPortionOrder(it)) })
        return meal
    }

    override fun generateGene(order: Order): Gene {

        val portionOrder = order as PortionOrder

        val portion = filteredFoodMap[portionOrder.filter]!![portionOrder.type][(Math.random() * filteredFoodMap[portionOrder.filter]!![portionOrder.type].size.toDouble()).toInt()].clone() as PortionItem
        with(seed as AlgorithmSeed) {
            portion.weight = (seed.foodWeighRange[portionOrder.type].start + Math.random() * (seed.foodWeighRange[portionOrder.type].endInclusive -
                    (seed.foodWeighRange[portionOrder.type].start))).toInt().times(seed.weightUnit).toInt()
        }
        return portion
    }

    private fun getPortionOrder(type: Int): Order = PortionOrder(type, getRandomFilter())

    private fun getRandomFilter(): Int = filters[(Math.random() * filters.size.toDouble()).toInt()]

    //ignore 1
    private fun getFilters(): List<Int> {
        val filters = mutableSetOf<Int>()
        seed.genePool.forEach { it.filter.forEach { if (it != 1) filters.add(it) } }
        return filters.toList()
    }

    private fun checkGene(geneFilters: List<Int>, filters: List<Int>): Boolean {
        var check = false
        geneFilters.forEach {
            if (filters.contains(it)) {
                check = true
            }
        }
        return check
    }

}