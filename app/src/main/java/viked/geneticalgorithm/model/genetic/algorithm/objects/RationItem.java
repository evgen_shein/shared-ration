package viked.geneticalgorithm.model.genetic.algorithm.objects;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import viked.algorithmlibrary.data.Chromosome;
import viked.algorithmlibrary.data.Individual;

/**
 * Created by Viked on 7/16/2016.
 */
public class RationItem implements IAlgorithmItem<Ration>, Individual {

    private double fitness;

    private List<Chromosome> mealList;

    @Override
    public void setRealmObject(Ration ration) {
        fitness = ration.getFitness();
        mealList = new ArrayList<>();
        for (Meal meal : ration.getMealRealmList()) {
            MealItem item = new MealItem();
            item.setRealmObject(meal);
            mealList.add(item);
        }
    }

    @Override
    public Ration getRealmObject(@NotNull Realm realm) {
        Ration ration = new Ration();
        ration.setFitness(fitness);
        for (Chromosome chromosome : mealList) {
            ration.addMeal(((MealItem) chromosome).getRealmObject(realm));
        }
        return ration;
    }

    @NotNull
    @Override
    public List<Chromosome> getGenotype() {
        if (mealList == null) {
            mealList = new ArrayList<>();
        }
        return mealList;
    }

    @Override
    public Individual clone() {
        RationItem rationItem = new RationItem();
        for (Chromosome chromosome : mealList) {
            rationItem.getGenotype().add(chromosome.clone());
        }
        rationItem.fitness = fitness;
        return rationItem;
    }

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }


    @NotNull
    @Override
    public double[] getValues() {
        double[] values = new double[]{0.0, 0.0, 0.0, 0.0};
        for (Chromosome item : mealList) {
            MealItem portion = (MealItem) item;
            double[] portionValues = portion.getValues();
            for (int i = 0; i < portionValues.length; i++) {
                values[i] += portionValues[i];
            }
        }
        return values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RationItem that = (RationItem) o;

        boolean equals = (Double.compare(that.fitness, fitness) == 0) && (mealList.size() == that.mealList.size());

        if (equals) {
            for (int i = mealList.size() - 1; i > -1; i--) {
                if (!mealList.get(i).equals(that.mealList.get(i))) {
                    equals = false;
                    break;
                }
            }
        }

        return equals;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(fitness);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (mealList != null ? mealList.hashCode() : 0);
        return result;
    }
}
