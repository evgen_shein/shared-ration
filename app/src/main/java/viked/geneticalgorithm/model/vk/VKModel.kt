package viked.geneticalgorithm.model.vk

import com.vk.sdk.api.*
import org.json.JSONArray
import org.json.JSONObject
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import viked.geneticalgorithm.MyApplication

/**
 * Created by Viked on 8/20/2016.
 */
class VKModel {

    init {
        MyApplication.graph.inject(this)
    }

    private val USER_REQUEST = "users.get"
    private val USER_SEX = "sex"
    private val USER_BDATE = "bdate"
    private val USER_PHOTO = "photo_200"


    private val GROUP_MEMBER_REQUEST = "groups.isMember"
    private val GROUP_MEMBER_KEY = "member"
    private val GROUP_JOIN_REQUEST = "groups.join"

    private val RESPONSE = "response"


    fun getUser(): Observable<JSONObject> {
        return Observable.create<JSONObject> {
            val request = VKRequest(USER_REQUEST, VKParameters.from(VKApiConst.FIELDS, "$USER_SEX,$USER_BDATE,$USER_PHOTO"))

            request.executeWithListener(object : VKRequest.VKRequestListener() {
                override fun onComplete(response: VKResponse?) {
                    if (response != null && response.json != null) {
                        val array = response.json.get(RESPONSE) as JSONArray
                        if (array.length() > 0) {
                            it.onNext(array.get(0) as JSONObject)
                        } else {
                            it.onError(Exception())
                        }
                    }
                    it.onCompleted()
                }

                override fun onError(error: VKError?) {
                    it.onError(Exception(error.toString()))
                }
            })
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun checkUserGroup(userId: String, groupId: String): Observable<Boolean> {
        return Observable.create<Boolean> {
            val params = VKParameters(mapOf(
                    Pair(VKApiConst.GROUP_ID, groupId),
                    Pair(VKApiConst.USER_ID, userId),
                    Pair(VKApiConst.EXTENDED, 1)))

            val request = VKRequest(GROUP_MEMBER_REQUEST, params)

            request.executeWithListener(object : VKRequest.VKRequestListener() {
                override fun onComplete(response: VKResponse?) {
                    if (response != null && response.json != null) {
                        val content = response.json.get(RESPONSE) as JSONObject
                        val isMember = content.get(GROUP_MEMBER_KEY) as Int
                        it.onNext(isMember == 1)
                    }
                    it.onCompleted()
                }

                override fun onError(error: VKError?) {
                    it.onError(Exception(error.toString()))
                }
            })
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun joinGroup(groupId: String): Observable<Boolean> {
        return Observable.create<Boolean> {
            val request = VKRequest(GROUP_JOIN_REQUEST, VKParameters.from(VKApiConst.GROUP_ID, groupId))

            request.executeWithListener(object : VKRequest.VKRequestListener() {
                override fun onComplete(response: VKResponse?) {
                    if (response != null && response.json != null) {
                        val result = response.json.get(RESPONSE) as Int
                        it.onNext(result == 1)
                    }
                    it.onCompleted()
                }

                override fun onError(error: VKError?) {
                    it.onError(Exception(error.toString()))
                }
            })
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

}