package viked.geneticalgorithm.utils;

import io.realm.Realm;
import viked.geneticalgorithm.model.data.objects.DBModel;

/**
 * Created by 1 on 09.07.2016.
 */
public class RealmUtils {

    private RealmUtils() {

    }

    public static boolean checkDBModel() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(DBModel.class).findFirst() != null;
    }

}
