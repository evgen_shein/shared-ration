package viked.algorithmlibrary.selection

import viked.algorithmlibrary.data.Individual

/**
 * Created by Viked on 8/28/2016.
 */
class MedianSelection(val median: Double) : ISelection {
    override fun isSuitable(individual: Individual): Boolean = individual.fitness < median
}