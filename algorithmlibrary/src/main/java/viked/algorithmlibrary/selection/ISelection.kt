package viked.algorithmlibrary.selection

import viked.algorithmlibrary.data.Individual

/**
 * Created by Viked on 8/28/2016.
 */
interface ISelection {
    fun isSuitable(individual: Individual): Boolean
}