package viked.algorithmlibrary

import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subjects.BehaviorSubject
import viked.algorithmlibrary.chooser.BaseStrategyChooser
import viked.algorithmlibrary.data.IAlgorithmState
import viked.algorithmlibrary.data.Population
import java.util.concurrent.TimeUnit

/**
 * Created by Viked on 8/25/2016.
 */
abstract class BaseAlgorithmModel : IAlgorithmModel {

    private var algorithmSubject: BehaviorSubject<IAlgorithmState> = BehaviorSubject.create()

    override fun run(): Observable<IAlgorithmState> {
        when {
            isComplete() -> algorithmSubject = BehaviorSubject.create()
            isRunning() -> return algorithmSubject
        }

        Observable.create<IAlgorithmState> {
            val strategyChooser = BaseStrategyChooser(getPopulation())
            while (strategyChooser.population.complete.not() && isComplete().not()) {

                val strategy = strategyChooser.get()
                while (strategyChooser.population.newIndividuals.size < strategyChooser.population.seed.populationSize) {
                    it.onNext(strategy.run(getStepCount(strategyChooser.population)))
                }
                strategyChooser.population.individuals.clear()
                strategyChooser.population.individuals.addAll(strategyChooser.population.newIndividuals)
                strategyChooser.population.newIndividuals.clear()
                strategyChooser.population.individuals.sortBy { it.fitness }

                if (isEndCondition(strategyChooser.population)) {
                    strategyChooser.population.complete = true
                }

                if (isComplete().not()) {
                    save(strategyChooser.population)
                    it.onNext(strategyChooser.population.getState())
                }
            }
            it.onCompleted()
        }.subscribeOn(Schedulers.io())
                .onBackpressureLatest()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(algorithmSubject)
        return algorithmSubject
    }

    override fun isComplete(): Boolean = algorithmSubject.hasCompleted()

    override fun isRunning(): Boolean = algorithmSubject.hasObservers()
            && algorithmSubject.hasCompleted().not()

    override fun stop() = algorithmSubject.onCompleted()


    private fun getStepCount(population: Population): Int {
        var count = population.seed.populationSize / 100
        if (population.newIndividuals.size + count > population.seed.populationSize) {
            count = population.seed.populationSize - population.newIndividuals.size
        }
        return count
    }
}