package viked.algorithmlibrary

import rx.Observable
import viked.algorithmlibrary.data.IAlgorithmState
import viked.algorithmlibrary.data.Population
import viked.algorithmlibrary.data.Seed

/**
 * Created by Viked on 8/25/2016.
 */
interface IAlgorithmModel {
    fun run() : Observable<IAlgorithmState>
    fun stop()

    fun isRunning(): Boolean
    fun isComplete(): Boolean

    fun getSeed(): Seed

    fun getPopulation(): Population

    fun isEndCondition(population: Population): Boolean

    fun save(population: Population)
}