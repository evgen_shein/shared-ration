package viked.algorithmlibrary.fitness

import viked.algorithmlibrary.data.Individual

/**
 * Created by Viked on 8/27/2016.
 */
interface IFitness {
    fun calculate(individual: Individual): Double
}