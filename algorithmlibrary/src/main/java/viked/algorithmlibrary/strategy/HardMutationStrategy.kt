package viked.algorithmlibrary.strategy

import viked.algorithmlibrary.data.IAlgorithmState
import viked.algorithmlibrary.data.Individual
import viked.algorithmlibrary.data.Population
import viked.algorithmlibrary.doForEach

/**
 * Created by Viked on 9/10/2016.
 */
class HardMutationStrategy(override val population: Population) : IStrategy {

    private val hardMutationCount = 0.75

    private var index = 1

    init {
        population.newIndividuals.add(population.individuals.first().clone())
    }

    override fun run(count: Int): IAlgorithmState {
        (0..count - 1)
                .map { population.individuals[index++].clone() }
                .doForEach { it.mutate() }
                .doForEach { it.fitness = population.seed.fitness.calculate(it) }
                .forEach { population.newIndividuals.add(it) }
        return population.getState()
    }

    private fun getMutationChromosomeCount(): Int {
        var count = 1 + (Math.random() * (population.seed.chromosomeCount.toDouble() * hardMutationCount)).toInt()
        if (count > population.seed.chromosomeCount) {
            count = population.seed.chromosomeCount
        }
        return count
    }


    private fun getChromosomeIndexes(count: Int): List<Int> {
        if (count == population.seed.chromosomeCount) {
            return Array(count, { it }).asList()
        }
        val set: MutableSet<Int> = mutableSetOf()
        while (set.size < count) {
            set.add((Math.random() * (population.seed.chromosomeCount - 1).toDouble()).toInt())
        }
        return set.toList()
    }

    private fun Individual.mutate() {
        getChromosomeIndexes(getMutationChromosomeCount())
                .forEach { this.genotype[it] = population.seed.initialisation.generateChromosome(population.getChromosomeOrder(it)) }
    }

}