package viked.algorithmlibrary.strategy

import viked.algorithmlibrary.data.IAlgorithmState
import viked.algorithmlibrary.data.Population
import viked.algorithmlibrary.doForEach

/**
 * Created by Viked on 8/30/2016.
 */
open class InitStrategy(override val population: Population) : IStrategy {

    override fun run(count: Int): IAlgorithmState {
        (0..count - 1)
                .map { population.seed.initialisation.generateIndividual() }
                .doForEach { it.fitness = population.seed.fitness.calculate(it) }
                .forEach { population.newIndividuals.add(it) }
        return population.getState()
    }
}