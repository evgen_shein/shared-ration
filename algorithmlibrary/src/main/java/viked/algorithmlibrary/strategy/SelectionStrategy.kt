package viked.algorithmlibrary.strategy

import viked.algorithmlibrary.data.IAlgorithmState
import viked.algorithmlibrary.data.Population
import viked.algorithmlibrary.doForEach
import viked.algorithmlibrary.reproduction.IReproduction
import viked.algorithmlibrary.selection.ISelection

/**
 * Created by Viked on 8/28/2016.
 */
class SelectionStrategy(population: Population, val selection: ISelection, val reproduction: IReproduction) : InitStrategy(population) {

    private val maxSteps = 10

    private var stepWithOutNewIndividuals = 0

    init {
        population.newIndividuals.add(population.individuals.first().clone())
    }

    override fun run(count: Int): IAlgorithmState {
        val size = population.newIndividuals.size

        if (stepWithOutNewIndividuals == maxSteps) {
            super.run(count)
        } else {
            (0..count - 1)
                    .map { reproduction.reproduction() }
                    .flatMap { it }
                    .doForEach { it.fitness = population.seed.fitness.calculate(it) }
                    .filter { selection.isSuitable(it) && population.newIndividuals.contains(it).not() }
                    .forEach {
                        population.newIndividuals.add(it)
                    }
        }

        if (size == population.newIndividuals.size) {
            stepWithOutNewIndividuals++
        } else {
            stepWithOutNewIndividuals = 0
        }

        return population.getState()
    }
}