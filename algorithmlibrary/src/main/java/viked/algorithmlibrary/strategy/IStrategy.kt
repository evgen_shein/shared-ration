package viked.algorithmlibrary.strategy

import viked.algorithmlibrary.data.IAlgorithmState
import viked.algorithmlibrary.data.Population

/**
 * Created by Viked on 8/27/2016.
 */
interface IStrategy {

    val population: Population

    fun run(count: Int): IAlgorithmState
}