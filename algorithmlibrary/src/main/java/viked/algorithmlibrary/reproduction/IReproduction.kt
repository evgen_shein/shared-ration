package viked.algorithmlibrary.reproduction

import viked.algorithmlibrary.data.Individual
import viked.algorithmlibrary.data.Population

/**
 * Created by Viked on 8/28/2016.
 */
interface IReproduction {

    val population: Population

    fun reproduction(): List<Individual>

}