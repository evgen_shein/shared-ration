package viked.algorithmlibrary.reproduction

import viked.algorithmlibrary.chooser.ICrossoverChooser
import viked.algorithmlibrary.data.Individual
import viked.algorithmlibrary.data.Population

/**
 * Created by Viked on 8/28/2016.
 */
class RankingReproduction(override val population: Population, val crossoverChooser: ICrossoverChooser) : IReproduction {

    private val count = 2

    private val probabilityList: List<Double> = (0..population.individuals.size - 1)
            .map { (2.0 - (2.0 - 0.0) * ((it.toDouble() - 1.0) / (population.individuals.size.toDouble() - 1.0))) / population.individuals.size.toDouble() }

    private fun getRandomListItemIndex(): Int {
        var cumulativeProbability = 0.0
        val index = Math.random()
        probabilityList.forEachIndexed { i, d ->
            cumulativeProbability += d
            if (index <= cumulativeProbability) {
                return i
            }
        }
        return 0
    }

    override fun reproduction(): List<Individual> {
        return crossoverChooser
                .get().cross((0..count - 1)
                .map { getRandomListItemIndex() }
                .map { population.individuals[it] })
    }

}