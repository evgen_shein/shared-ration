package viked.algorithmlibrary.crossover

import viked.algorithmlibrary.data.Chromosome
import viked.algorithmlibrary.data.Gene
import viked.algorithmlibrary.data.Individual

/**
 * Created by Viked on 8/25/2016.
 */
abstract class BaseSimpleCrossover : ICrossover {
    override fun cross(individuals: List<Individual>): List<Individual> {
        val copy = individuals.map { it.clone()}
        getIndividualIndexes(copy).forEach { changeChromosome(Pair(individuals[0].genotype[it.first], individuals[1].genotype[it.second])) }
        return copy
    }

    private fun changeChromosome(pair: Pair<Chromosome, Chromosome>) {
        val temp = mutableListOf<Gene>()
        temp.addAll(pair.first.genotype)
        pair.first.genotype.clear()
        pair.first.genotype.addAll(pair.second.genotype)
        pair.second.genotype.clear()
        pair.second.genotype.addAll(temp)
    }

}