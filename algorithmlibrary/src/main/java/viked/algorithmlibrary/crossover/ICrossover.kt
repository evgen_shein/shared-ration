package viked.algorithmlibrary.crossover

import viked.algorithmlibrary.data.Individual

/**
 * Created by Viked on 8/25/2016.
 */
interface ICrossover {
    fun cross(individuals: List<Individual>): List<Individual>
    fun getIndividualIndexes(individuals: List<Individual>): List<Pair<Int, Int>>
}