package viked.algorithmlibrary.crossover

import viked.algorithmlibrary.data.Individual

/**
 * Created by Viked on 8/27/2016.
 */
class TwoPointCrossover : BaseSimpleCrossover() {
    override fun getIndividualIndexes(individuals: List<Individual>): List<Pair<Int, Int>> {
        val firstPoint = (Math.random() * (individuals[0].genotype.size - 1).toDouble()).toInt()
        val secondPoint = ((firstPoint.toDouble() + Math.random()) * ((individuals[0].genotype.size - 1).toDouble() - firstPoint.toDouble())).toInt()
        return (firstPoint..secondPoint).map { Pair(it, it) }
    }
}