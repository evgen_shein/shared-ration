package viked.algorithmlibrary.crossover

import viked.algorithmlibrary.data.Individual

/**
 * Created by Viked on 8/27/2016.
 */
class OnePointCrossover : BaseSimpleCrossover() {
    override fun getIndividualIndexes(individuals: List<Individual>): List<Pair<Int, Int>> {
        val point = (Math.random() * (individuals[0].genotype.size - 1).toDouble()).toInt()
        return (point..individuals[0].genotype.size - 1).map { Pair(it, it) }
    }
}