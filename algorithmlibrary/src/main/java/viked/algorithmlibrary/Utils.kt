package viked.algorithmlibrary

/**
 * Created by Viked on 9/8/2016.
 */
inline fun <T> Iterable<T>.doForEach(action: (T) -> Unit): Iterable<T> {
    for (element in this) action(element)
    return this
}

inline fun <T> Iterable<T>.doForEachIndexed(action: (Int, T) -> Unit): Iterable<T> {
    var index = 0
    for (element in this) action(index++, element)
    return this
}