package viked.algorithmlibrary.chooser

import viked.algorithmlibrary.crossover.OnePointCrossover
import viked.algorithmlibrary.crossover.TwoPointCrossover
import viked.algorithmlibrary.data.Population
import viked.algorithmlibrary.reproduction.RankingReproduction
import viked.algorithmlibrary.selection.MedianSelection
import viked.algorithmlibrary.strategy.HardMutationStrategy
import viked.algorithmlibrary.strategy.IStrategy
import viked.algorithmlibrary.strategy.InitStrategy
import viked.algorithmlibrary.strategy.SelectionStrategy

/**
 * Created by Viked on 8/27/2016.
 */
class BaseStrategyChooser(override val population: Population) : IStrategyChooser {

    private val iterationsWithOutProgressRate = 2.5

    private var lastFitness = 0.0
    private var iterationsWithOutProgress = 0

    override fun get(): IStrategy {
        population.iteration++

        if (population.individuals.isNotEmpty()) {
            val fitness = population.getMinFitness()
            if (lastFitness == fitness) {
                iterationsWithOutProgress++
            } else {
                lastFitness = fitness
                iterationsWithOutProgress = 0
            }
        }

        return when {
            population.individuals.isEmpty() -> InitStrategy(population)
            Math.random() < iterationsWithOutProgress.toDouble() / 100.0 * iterationsWithOutProgressRate -> {
                iterationsWithOutProgress = 0
                HardMutationStrategy(population)
            }
            else -> SelectionStrategy(population, MedianSelection(population.getMedianFitness()), RankingReproduction(population, getCrossoverChooser()))
        }
    }

    private fun getCrossoverChooser(): ICrossoverChooser {
        return RandomCrossoverChooser(listOf(OnePointCrossover(), TwoPointCrossover()))
    }
}