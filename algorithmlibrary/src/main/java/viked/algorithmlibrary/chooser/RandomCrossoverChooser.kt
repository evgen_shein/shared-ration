package viked.algorithmlibrary.chooser

import viked.algorithmlibrary.crossover.ICrossover

/**
 * Created by Viked on 8/28/2016.
 */
class RandomCrossoverChooser(override val crossovers: List<ICrossover>) : ICrossoverChooser {

    override fun get(): ICrossover = crossovers[(Math.random() * (crossovers.size - 1).toDouble()).toInt()]

}