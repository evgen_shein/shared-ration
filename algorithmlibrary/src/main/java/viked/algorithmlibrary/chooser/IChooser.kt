package viked.algorithmlibrary.chooser

/**
 * Created by Viked on 8/28/2016.
 */
interface IChooser<out ITEM> {
    fun get(): ITEM
}