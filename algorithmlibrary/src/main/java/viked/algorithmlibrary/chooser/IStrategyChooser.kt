package viked.algorithmlibrary.chooser

import viked.algorithmlibrary.data.Population
import viked.algorithmlibrary.data.Seed
import viked.algorithmlibrary.strategy.IStrategy

/**
 * Created by Viked on 8/27/2016.
 */
interface IStrategyChooser : IChooser<IStrategy>{

    val population: Population

}