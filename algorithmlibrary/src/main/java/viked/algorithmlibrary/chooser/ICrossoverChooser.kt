package viked.algorithmlibrary.chooser

import viked.algorithmlibrary.crossover.ICrossover

/**
 * Created by Viked on 8/28/2016.
 */
interface ICrossoverChooser : IChooser<ICrossover> {

    val crossovers: List<ICrossover>

}