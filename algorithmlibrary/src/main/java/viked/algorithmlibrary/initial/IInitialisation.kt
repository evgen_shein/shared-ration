package viked.algorithmlibrary.initial

import viked.algorithmlibrary.data.*

/**
 * Created by Viked on 8/27/2016.
 */
interface IInitialisation {

    val seed: Seed

    fun generateIndividual(): Individual

    fun generateChromosome(order: Order): Chromosome

    fun generateGene(order: Order): Gene

}