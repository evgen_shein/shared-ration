package viked.algorithmlibrary.initial

import viked.algorithmlibrary.data.Chromosome
import viked.algorithmlibrary.data.Gene
import viked.algorithmlibrary.data.Individual
import viked.algorithmlibrary.data.Seed

/**
 * Created by Viked on 8/28/2016.
 */
abstract class AbstractInitialisation(override val seed: Seed) : IInitialisation {

    /*
    abstract fun newIndividualInstance(): Individual

    abstract fun newChromosomeInstance(): Chromosome

    abstract fun newGeneInstance(): Gene

    abstract fun initialGeneParams(gene: Gene)

    override fun generateIndividual(): Individual {
        val individual = newIndividualInstance()
        individual.genotype.clear()
        individual.genotype.addAll((0..seed.chromosomeCount - 1)
                .map { generateChromosome() })
        return individual
    }

    override fun generateChromosome(): Chromosome {
        val chromosome = newChromosomeInstance()
        chromosome.genotype.clear()
        chromosome.genotype.addAll((0..seed.geneCount - 1)
                .map { generateGene() })
        return chromosome
    }

    override fun generateGene(): Gene {
        val gene = newGeneInstance()
        initialGeneParams(gene)
        return gene
    }
    */
}