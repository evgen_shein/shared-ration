package viked.algorithmlibrary.data

/**
 * Created by Viked on 8/27/2016.
 */
interface Population {

    val seed: Seed

    var iteration: Int

    val individuals: MutableList<Individual>

    val newIndividuals: MutableList<Individual>

    var complete: Boolean

    fun getState(): IAlgorithmState

    fun getMinFitness(): Double
    fun getMaxFitness(): Double
    fun getMedianFitness(): Double

    fun getChromosomeOrder(type: Int): Order

}