package viked.algorithmlibrary.data

import viked.algorithmlibrary.fitness.IFitness
import viked.algorithmlibrary.initial.IInitialisation
import viked.algorithmlibrary.reproduction.IReproduction
import viked.algorithmlibrary.selection.ISelection

/**
 * Created by Viked on 8/25/2016.
 */
interface Seed {

    val savePopulationSize: Int

    val populationSize: Int

    val fitness: IFitness

    val genePool: List<Gene>

    val chromosomeCount: Int

    val geneCount: Int

    val filter: List<Int>

    val initialisation: IInitialisation

    val error: Double

}