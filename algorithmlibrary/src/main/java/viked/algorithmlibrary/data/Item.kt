package viked.algorithmlibrary.data

/**
 * Created by Viked on 8/25/2016.
 */
interface Item<out T> {
    fun clone(): T
}