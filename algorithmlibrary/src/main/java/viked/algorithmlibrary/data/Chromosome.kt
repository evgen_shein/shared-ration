package viked.algorithmlibrary.data

/**
 * Created by Viked on 8/25/2016.
 */
interface Chromosome : Item<Chromosome> {
    val type: Int
    val genotype: MutableList<Gene>
}