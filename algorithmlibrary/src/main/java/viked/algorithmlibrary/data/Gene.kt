package viked.algorithmlibrary.data

/**
 * Created by Viked on 8/25/2016.
 */
interface Gene : Item<Gene> {
    val type: Int
    val filter: List<Int>
}