package viked.algorithmlibrary.data

/**
 * Created by Viked on 8/25/2016.
 */
interface Individual : Item<Individual> {
    val genotype: MutableList<Chromosome>
    var fitness : Double
}