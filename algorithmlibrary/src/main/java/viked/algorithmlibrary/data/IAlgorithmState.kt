package viked.algorithmlibrary.data

/**
 * Created by Viked on 8/27/2016.
 */
interface IAlgorithmState {

    val complete: Boolean

    val iterationProgress: Double

    val iterationNumber: Int

}